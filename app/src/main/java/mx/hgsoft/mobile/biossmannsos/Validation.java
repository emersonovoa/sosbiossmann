package mx.hgsoft.mobile.biossmannsos;

import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by resident on 03/05/16.
 */
public class Validation {

    // Error Messages
    private static final String REQUIRED_MSG = "Llenar";
    private static final String INCORRECT_DATE = "Corregir fecha de salida";
    private static final String EMAIL_MSG = "Email inválido";

    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static boolean isEmailAddress(EditText editText, boolean required) {
        return isValid(editText, EMAIL_REGEX, EMAIL_MSG, required);
    }

    // Return true if the input field is valid, based on the parameter passed
    public static boolean isValid(EditText editText, String regex, String errMsg, boolean required) {
        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editText.setError(null);
        // text required and editText is blank, so return false
        if ( required && !hasText(editText) ) return false;
        // pattern doesn't match so returning false
        if (required && !Pattern.matches(regex, text)) {
            editText.setError(errMsg);
            return false;
        };

        return true;
    }

    // check the input field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasText(EditText editText) {
        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            editText.setError(REQUIRED_MSG);
            return false;
        }
        return true;
    }

    // check the autocomplete field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasTextAutocomplete(AutoCompleteTextView autoCompleteTextView) {
        String text = autoCompleteTextView.getText().toString().trim();
        autoCompleteTextView.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            autoCompleteTextView.setError(REQUIRED_MSG);
            return false;
        }
        return true;
    }

    // check the input field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasString(String text) {
        // length 0 means there is no text
        if (text.length() == 0) {
            return false;
        }
        return true;
    }

    public static boolean selectedElementSpinner(Spinner spinner) {
        int element = spinner.getSelectedItemPosition();

        if (element == 0) {
            return false;
        }
        return true;
    }

    public static boolean selectedElementsSpinners(Spinner spinnerAO, Spinner spinnerAD) {
        int elementAO = spinnerAO.getSelectedItemPosition();
        int elementAD = spinnerAD.getSelectedItemPosition();

        if (elementAO == elementAD) {
            return false;
        }
        return true;
    }

    public static boolean compareDates(EditText editText, Date dateOne, Date dateTwo) {
        String text = editText.getText().toString().trim();
        editText.setError(null);
        long timeOne = dateOne.getTime();
        long timeTwo = dateTwo.getTime();
        long oneDay = 1000 * 60 * 60 * 24;
        long delta = (timeTwo - timeOne) / oneDay;
        if (delta > 0) {
            return true;
        } else {
            delta *= -1;
            editText.setError(INCORRECT_DATE);
            return false;
        }
    }
}

