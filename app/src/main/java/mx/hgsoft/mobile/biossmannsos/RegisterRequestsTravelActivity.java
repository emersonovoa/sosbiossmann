package mx.hgsoft.mobile.biossmannsos;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 05/05/16.
 */
public class RegisterRequestsTravelActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();

    private EditText inputFolio;
    private Spinner spinnerCuentas;
    private Spinner spinnerZonas;
    private EditText inputFechaSalida;
    private EditText inputFechaRegreso;
    private EditText inputNoPersonas;
    private Spinner spinnerTransportes;
    private EditText inputObservaciones;
    private Button btnSave;

    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private SessionManager session;

    private Date date_fechasalida = new Date();
    private Date date_fecharegreso = new Date();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_requeststravel);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        getLastRecordRequestTravel();
        getCatCounts();
        getCatZones();

        /* Getting xml elements */
        inputFolio = (EditText) findViewById(R.id.folio);

        inputFechaSalida = (EditText) findViewById(R.id.fecha_salida);
        inputFechaSalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerOut();
            }
        });

        inputFechaRegreso = (EditText) findViewById(R.id.fecha_regreso);
        inputFechaRegreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerIn();
            }
        });

        inputNoPersonas = (EditText) findViewById(R.id.no_personas);

        getCatTransports();

        inputObservaciones = (EditText) findViewById(R.id.observaciones);

        btnSave = (Button) findViewById(R.id.btnRegisterRequestTravel);

        // Register Button Click event
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Fetching user details from sqlite
                HashMap<String, String> user = db.getUserDetails();

                String id_st = user.get("uid");
                String folio = inputFolio.getText().toString().trim();
                /* Spinner counts */
                String spinnerCountsString = null;
                spinnerCountsString = spinnerCuentas.getSelectedItem().toString();
                int nPosCountsSpinner = spinnerCuentas.getSelectedItemPosition();
                String nPosSpinnerCountsString = Integer.toString(nPosCountsSpinner);
                /* End spinner counts */
                /* Spinner zones */
                String spinnerZonesString = null;
                spinnerZonesString = spinnerZonas.getSelectedItem().toString();
                int nPosZonesSpinner = spinnerZonas.getSelectedItemPosition();
                String nPosSpinnerZonesString = Integer.toString(nPosZonesSpinner);
                /* End spinner zones */
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
                String fecha_salida = inputFechaSalida.getText().toString().trim();
                try {
                    date_fechasalida = sdf.parse(fecha_salida);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String fecha_regreso = inputFechaRegreso.getText().toString().trim();
                try {
                    date_fecharegreso = sdf.parse(fecha_regreso);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String no_noches = getDateDiffString(date_fechasalida, date_fecharegreso);
                String no_personas = inputNoPersonas.getText().toString().trim();
                /* Spinner transports */
                String spinnerTransportsString = null;
                spinnerTransportsString = spinnerTransportes.getSelectedItem().toString();
                int nPosTransportsSpinner = spinnerTransportes.getSelectedItemPosition();
                String nPosSpinnerTransportsString = Integer.toString(nPosTransportsSpinner);
                /* End spinner transports */
                String observaciones = inputObservaciones.getText().toString().trim();
                // Fetching user details from sqlite
                String usrMod = user.get("email");
                WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
                String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                if (checkValidation()) {
                    registerRequestsTravel(id_st, folio, nPosSpinnerCountsString, nPosSpinnerZonesString, fecha_salida, fecha_regreso, no_noches, no_personas, nPosSpinnerTransportsString, observaciones, usrMod, ip);
                    db.deleteLastRecordRequestTravel();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Llenar correctamente los campos!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), ListViewRequestsTravelActivity.class);
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        db.deleteLastRecordRequestTravel();
        startActivityForResult(myIntent, 0);
        return true;
    }

    private void showDatePickerOut() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondateOut);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondateOut = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            inputFechaSalida.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(dayOfMonth));
        }
    };

    private void showDatePickerIn() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondateIn);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondateIn = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            inputFechaRegreso.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(dayOfMonth));

        }
    };

    public String getDateDiffString(Date dateOne, Date dateTwo)
    {
        long timeOne = dateOne.getTime();
        long timeTwo = dateTwo.getTime();
        long oneDay = 1000 * 60 * 60 * 24;
        long delta = (timeTwo - timeOne) / oneDay;

        if (delta > 0) {
            return "" + delta + "";
        }
        else {
            delta *= -1;
            return "" + delta + "";
        }
    }

    /**
     * function to get JSON from API_REST about last record from Requests Travel
     */
    private void getLastRecordRequestTravel() {
        String tag_string_req = "req_lastRecordRequestsTravel";

        pDialog.setMessage("");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REQUESTSTRAVELLAST, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Last record response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("last_record");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String folio_id = jsonObject.optString("ID_Folio").toString();
                            String folio = jsonObject.optString("Folio").toString();

                            // Inserting row in orders table
                            db.addLastRecordRequestTravel(folio_id, folio);
                        }

                        setNumberFolio();

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to get JSON from API_REST about Catalog's counts
     */
    private void getCatCounts() {
        String tag_string_req = "req_catCounts";

        pDialog.setMessage("");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CATCOUNTS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Counts response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("counts");

                        db.addCatCounts("0", "Elige cuenta");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String cuentaid = jsonObject.optString("ID_Cuenta").toString();
                            String cuenta = jsonObject.optString("Cuenta").toString();

                            // Inserting row in orders table
                            db.addCatCounts(cuentaid, cuenta);
                        }
                        populatingSpinnerCounts();

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to get JSON from API_REST about Catalog's zones
     */
    private void getCatZones() {
        String tag_string_req = "req_catZones";

        pDialog.setMessage("");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CATZONES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Zones response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("zones");

                        db.addCatZones("0", "Elige zona");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String zonaid = jsonObject.optString("ID_Zona").toString();
                            String zona = jsonObject.optString("Zona").toString();

                            // Inserting row in orders table
                            db.addCatZones(zonaid, zona);
                        }
                        populatingSpinnerZones();

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to get JSON from API_REST about Catalog's transports
     */
    private void getCatTransports() {
        String tag_string_req = "req_catTransports";

        pDialog.setMessage("");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_KINDTRANSPORTS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Transports response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("transports");

                        db.addCatTransports("0", "Elige transporte");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String transporteid = jsonObject.optString("ID_TipoTransporte").toString();
                            String transporte = jsonObject.optString("Tipo_Transporte").toString();

                            // Inserting row in orders table
                            db.addCatTransports(transporteid, transporte);
                        }
                        populatingSpinnerTransports();

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void setNumberFolio() {
        HashMap<String, String> last_folio = db.getLastFolio();
        String folio_record = last_folio.get("lr_rt_folio");
        String sv = "SV";
        String folio_str;
        if (folio_record.length() == 4) {
            folio_str = sv + "00" + folio_record;
        }else {
            folio_str = sv + "0" + folio_record;
        }
        inputFolio.setText(folio_str);
        System.out.println("FOLIO: " + folio_str);
    }

    private void populatingSpinnerCounts() {

            /* To populate Spinner, first create an adapter */
        spinnerCuentas = (Spinner) findViewById(R.id.cuenta_spinner);

        // The desired columns to be bound
        String[] columns = new String[]{
                "count"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[]{
                android.R.id.text1
        };

        Cursor cursorCounts = db.getCountsListCursor();

        SimpleCursorAdapter adapterCounts = new SimpleCursorAdapter(
                this, // context
                android.R.layout.simple_spinner_item, // layout file
                cursorCounts, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterCounts.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerCuentas.setAdapter(adapterCounts);
    }

    private void populatingSpinnerZones() {
        spinnerZonas = (Spinner) findViewById(R.id.zona_spinner);
        String[] columns = new String[]{
                "zone"
        };
        int[] to = new int[]{
                android.R.id.text1
        };
        Cursor cursorZones = db.getZonesListCursor();

        SimpleCursorAdapter adapterZones = new SimpleCursorAdapter(
                this, // context
                android.R.layout.simple_spinner_item, // layout file
                cursorZones, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterZones.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerZonas.setAdapter(adapterZones);
    }

    private void populatingSpinnerTransports() {
        spinnerTransportes = (Spinner) findViewById(R.id.tipotransporte_spinner);
        String[] columns = new String[]{
                "transport"
        };
        int[] to = new int[]{
                android.R.id.text1
        };
        Cursor cursorTransports = db.getTransportsListCursor();

        SimpleCursorAdapter adapterTransports = new SimpleCursorAdapter(
                this, // context
                android.R.layout.simple_spinner_item, // layout file
                cursorTransports, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterTransports.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerTransportes.setAdapter(adapterTransports);
    }

    /**
     * Function to store requeststravel in MySQL database will post
     * params(folio, nPosSpinnerCountsString, nPosSpinnerZonesString,
     * fecha_salida, fecha_regreso, no_personas, nPosSpinnerTransportsString,
     * observaciones) to register url
     * */
    private void registerRequestsTravel(final String id_st, final String folio, final String nPosSpinnerCountsString, final String nPosSpinnerZonesString,
                                final String fecha_salida, final String fecha_regreso, final String no_noches, final String no_personas,
                                final String nPosSpinnerTransportsString, final String observaciones, final String usrMod, final String ip) {
        // Tag used to cancel the request
        String tag_string_req = "req_registerRequestsTravel";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTERREQUESTSTRAVEL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Repairs Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // Repair successfully stored in MySQL
                        Toast.makeText(getApplicationContext(), "Inserción Exitosa", Toast.LENGTH_LONG).show();
                        db.deleteCounts();
                        db.deleteZones();
                        db.deleteTransports();
                        // Launch ManageOrderActivity activity
                        Intent intent = new Intent(
                                RegisterRequestsTravelActivity.this,
                                ListViewRequestsTravelActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                //Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_st", id_st);
                params.put("folio", folio);
                params.put("cuenta", nPosSpinnerCountsString);
                params.put("zona", nPosSpinnerZonesString);
                params.put("fecha_salida", fecha_salida);
                params.put("fecha_regreso", fecha_regreso);
                params.put("no_noches", no_noches);
                params.put("no_personas", no_personas);
                params.put("tipo_transporte", nPosSpinnerTransportsString);
                params.put("observaciones", observaciones);
                params.put("usrMod", usrMod);
                params.put("ip", ip);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText(inputFolio)) ret = false;
        if (!Validation.selectedElementSpinner(spinnerCuentas)) {
            Toast.makeText(this, "Seleccionar cuenta", Toast.LENGTH_SHORT).show();
            ret = false;
        }
        if (!Validation.selectedElementSpinner(spinnerZonas)) {
            Toast.makeText(this, "Seleccionar zona", Toast.LENGTH_SHORT).show();
            ret = false;
        }
        if (!Validation.hasText(inputFechaSalida)) ret = false;
        if (!Validation.hasText(inputFechaRegreso)) ret = false;
        if (!Validation.compareDates(inputFechaSalida, date_fechasalida, date_fecharegreso)) ret = false;
        if (!Validation.hasText(inputNoPersonas)) ret = false;
        if (!Validation.selectedElementSpinner(spinnerTransportes)) {
            Toast.makeText(this, "Seleccionar transporte", Toast.LENGTH_SHORT).show();
            ret = false;
        }
        return ret;
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        db.deleteLastRecordRequestTravel();
        // Launching the login activity
        Intent intent = new Intent(RegisterRequestsTravelActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }

}