package mx.hgsoft.mobile.biossmannsos.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.hgsoft.mobile.biossmannsos.SKURepairs;

/**
 * Created by resident on 14/03/16.
 */

public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 74;

    // Database Name
    private static final String DATABASE_NAME = "android_api";

    // Login table name
    private static final String TABLE_USER = "user";
    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_FOTO = "foto";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_UID = "uid";
    private static final String KEY_CREATED_AT = "created_at";

    // Orders table name
    private static final String TABLE_ORDERS = "orders";
    // Orders Table Columns names
    public static final String KEY_ID_ORDER = "_id";
    private static final String KEY_ORDENID = "ordenid";
    private static final String KEY_IDTIPOSERVICIO = "idtiposervicio";
    private static final String KEY_TIPOSERVICIO = "tiposervicio";
    private static final String KEY_INDEX = "index_orden";
    private static final String KEY_SERIE = "serie";
    private static final String KEY_FOLIO = "folio";
    private static final String KEY_NOORDEN = "no_orden";
    private static final String KEY_FECHAORDEN = "fecha_orden";
    private static final String KEY_ID_CE = "id_ce";
    private static final String KEY_CATEGORIA = "categoria";
    private static final String KEY_IDCONTACTO = "id_contacto";
    private static final String KEY_FECHAINICIO = "fecha_inicio";
    private static final String KEY_FECHAFIN = "fecha_fin";
    private static final String KEY_FALLAREPORTADA = "falla_reportada";
    private static final String KEY_NOMBREUSUARIO = "nombre_usuario";
    private static final String KEY_NOMBRESUPERVISOR = "nombre_supervisor";
    private static final String KEY_ANEXO = "anexo";
    private static final String KEY_FUNCIONANDO = "funcionando";
    private static final String KEY_OBSERVACIONES = "observaciones";
    private static final String KEY_STATUS = "status";
    private static final String KEY_FECHACIERRE = "fecha_cierre";
    private static final String KEY_FECHAALTA = "fecha_alta";
    private static final String KEY_USRALTA = "usr_alta";
    private static final String KEY_USRMOD = "usr_mod";
    private static final String KEY_SKU = "sku";
    private static final String KEY_SKUFABRICANTE = "sku_fabricante";
    private static final String KEY_MODELO = "modelo";
    private static final String KEY_INTEGREADO = "integrado";
    private static final String KEY_SKU_PADRE = "sku_padre";
    private static final String KEY_SKUFABRICANTE_PADRE = "sku_fabricante_padre";
    private static final String KEY_MODELO_PADRE = "modelo_padre";

    // Routines Attended table name
    private static final String TABLE_ROUTINES = "routines";
    // Routines Table Columns names
    public static final String KEY_ID_ROUTINE = "_id";
    private static final String KEY_ROUTINEID = "routineid";
    private static final String KEY_TIPOSERVICIOID = "tiposervicioid";
    private static final String KEY_MODEL = "modelo";
    private static final String KEY_ORDEN = "orden";
    private static final String KEY_RUTINA = "rutina";
    private static final String KEY_REVISAR = "revisar";
    private static final String KEY_COLOR = "color";

    // Have Routine?
    private static final String TABLE_HAVEROUTINE = "haveroutine";
    // Routines Table Columns names
    public static final String KEY_ID_HAVEROUTINE = "_id";
    private static final String KEY_HRTIPOSERVICIOID = "hrtiposervicioid";
    private static final String KEY_HRTIPOSERVICIO = "hrtiposervicio";
    private static final String KEY_HRRUTINA = "hrrutina";

    // Repairs table name
    private static final String TABLE_REPAIRS = "repairs";
    // Repairs Table Columns names
    public static final String KEY_ID_REPAIR = "_id";
    private static final String KEY_REPAIRID = "repairid";
    private static final String KEY_ORDERID = "orderid";
    private static final String KEY_CANTIDAD = "cantidad";
    private static final String KEY_SKU_R = "sku_r";
    private static final String KEY_UNITARIO = "unitario";
    private static final String KEY_TOTAL = "total";
    private static final String KEY_ACTIVO = "activo";
    private static final String KEY_FECHA_MOD = "fecha_mod";

    // Requests Travel table name
    private static final String TABLE_REQUESTS_TRAVEL = "requests_travel";
    // Requests Travel Table Columns names
    public static final String KEY_ID_REQUESTS_TRAVEL = "_id";
    private static final String KEY_REQUESTSTRAVELID = "svid";
    private static final String KEY_FOLIORT = "foliort";
    private static final String KEY_FECHASOLICITUD = "fecha_solicitud";
    private static final String KEY_CUENTA = "cuenta";
    private static final String KEY_ID_ZONA = "id_zona";
    private static final String KEY_ZONA = "zona";
    private static final String KEY_FECHASALIDA = "fecha_salida";
    private static final String KEY_FECHAREGRESO = "fecha_regreso";
    private static final String KEY_NONOCHES = "no_noches";
    private static final String KEY_NOPERSONAS = "no_personas";
    private static final String KEY_TIPOTRANSPORTE = "tipo_transporte";
    private static final String KEY_ANTICIPO = "anticipo";
    private static final String KEY_GASTOSAVION = "gastos_avion";
    private static final String KEY_GASTOSCOMPROBADOS = "gastos_comprobados";
    private static final String KEY_OBSERVACIONESRT = "observacionesrt";
    private static final String KEY_STATUSRT = "statustr";
    private static final String KEY_FECHAMOD = "fecha_mod";
    private static final String KEY_USRMODRT = "usr_modrt";
    private static final String KEY_IPMOD = "ip_mod";

    // Requests Travel Details table name
    private static final String TABLE_REQUESTS_TRAVEL_DETAILS = "requests_travel_details";
    // Requests Travel Details Table Columns names
    public static final String KEY_ID_REQUESTS_TRAVEL_DETAILS = "_id";
    private static final String KEY_REQUESTSTRAVELDETAILSDID = "svdid";
    private static final String KEY_REQUESTSTRAVELDETAILSID = "svid";
    private static final String KEY_TIPOVIATICO = "tipo_viatico";
    private static final String KEY_UNITARIOD = "unitariod";
    private static final String KEY_TOTALD = "totald";
    private static final String KEY_OBSERVACIONESRTD = "observacionesrtd";

    // Requests Travel Tickets table name
    private static final String TABLE_REQUESTS_TRAVEL_TICKETS = "requests_travel_tickets";
    // Requests Travel Tickets Table Columns names
    public static final String KEY_ID_REQUESTS_TRAVEL_TICKETS = "_id";
    private static final String KEY_REQUESTSTRAVELTICKETSSBAID = "sbaid";
    private static final String KEY_REQUESTSTRAVELTICKETSVID = "tsvid";
    private static final String KEY_ID_AO = "id_ao";
    private static final String KEY_AEROORIGEN = "aero_origen";
    private static final String KEY_ID_AD = "id_ad";
    private static final String KEY_AERODESTINO = "aero_destino";
    private static final String KEY_FECHA_SALIDA = "fecha_salida";
    private static final String KEY_HORA_SALIDA = "hora_salida";
    private static final String KEY_CLAVE_RESERVACION = "clave_reservacion";
    private static final String KEY_COSTO = "costo";

    // Requests Travel Vouchers table name
    private static final String TABLE_REQUESTS_TRAVEL_VOUCHERS = "requests_travel_vouchers";
    // Requests Travel Vouchers Table Columns names
    public static final String KEY_ID_REQUESTS_TRAVEL_VOUCHERS = "_id";
    private static final String KEY_REQUESTSTRAVELVOUCHERSID = "vid";
    private static final String KEY_REQUESTSTRAVELVOUCHERSSVID = "vsvid";
    private static final String KEY_ID_TIPO_VIATICO = "id_tipo_viatico";
    private static final String KEY_TIPO_VIATICO = "tipo_viatico";
    private static final String KEY_ID_TIPO_COMPROBANTE = "id_tipo_comprobante";
    private static final String KEY_TIPO_COMPROBANTE = "tipo_comprobante";
    private static final String KEY_SERIE_V = "serie_v";
    private static final String KEY_FOLIO_V = "folio_v";
    private static final String KEY_FECHA_V = "fecha_v";
    private static final String KEY_RFC_PROVEEDOR = "rfc_proveedor";
    private static final String KEY_MONEDA = "moneda";
    private static final String KEY_TOTAL_V = "total_v";
    private static final String KEY_XML = "xml_v";
    private static final String KEY_PDF = "pdf_v";
    private static final String KEY_STATUS_V = "status_v";

    // Last Record Request Travel table name
    private static final String TABLE_LR_RT = "last_record_req_travel";
    // Counts Table Columns names
    public static final String KEY_ID_LR_RT = "_id";
    private static final String KEY_LR_RTID = "lr_rtid";
    private static final String KEY_LR_RT_FOLIO = "lr_rt_folio";

    // Catalogs
    // Counts table name
    private static final String TABLE_COUNTS = "counts";
    // Counts Table Columns names
    public static final String KEY_ID_COUNT = "_id";
    private static final String KEY_COUNTID = "countid";
    private static final String KEY_COUNT = "count";

    // Zones table name
    private static final String TABLE_ZONES = "zones";
    // Zones Table Columns names
    public static final String KEY_ID_ZONE = "_id";
    private static final String KEY_ZONEID = "zoneid";
    private static final String KEY_ZONE = "zone";

    // Transports table name
    private static final String TABLE_TRANSPORTS = "transports";
    // Transports Table Columns names
    public static final String KEY_ID_TRANSPORT = "_id";
    private static final String KEY_TRANSPORTID = "transportid";
    private static final String KEY_TRANSPORT = "transport";

    // Travels table name
    private static final String TABLE_TRAVELS = "travels";
    // Travels Table Columns names
    public static final String KEY_ID_TRAVEL = "_id";
    private static final String KEY_TRAVELID = "travelid";
    private static final String KEY_TRAVEL = "travel";

    // Airports table name
    private static final String TABLE_AIRPORTS = "airports";
    // Travels Table Columns names
    public static final String KEY_ID_AIRPORTS = "_id";
    private static final String KEY_AIRPORTID = "airportid";
    private static final String KEY_AIRPORT = "airport";

    // Vouchers table name
    private static final String TABLE_VOUCHERS = "vouchers";
    // Travels Table Columns names
    public static final String KEY_ID_VOUCHERS = "_id";
    private static final String KEY_VOUCHERID = "voucherid";
    private static final String KEY_VOUCHER = "voucher";

    // Providers table name
    private static final String TABLE_PROVIDERS = "providers";
    // Travels Table Columns names
    public static final String KEY_ID_PROVIDERS = "_id";
    private static final String KEY_PROVIDERID = "providerid";
    private static final String KEY_RFC_PROVIDER = "rfc_provider";
    private static final String KEY_NAME_PROVIDER = "name_provider";

    // SKU Repairs table name
    private static final String TABLE_SKU_REPAIRS = "sku_repairs";
    // Travels Table Columns names
    public static final String KEY_ID_SKU_REPAIRS = "_id";
    private static final String KEY_SKU_REPAIR = "skurepair";
    private static final String KEY_DESC_SKU_REPAIRS = "desc_sku_reapir";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_FOTO + " TEXT," +
                KEY_NAME + " TEXT," +
                KEY_EMAIL + " TEXT UNIQUE," +
                KEY_UID + " TEXT," +
                KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        String CREATE_ORDERS_TABLE = "CREATE TABLE " + TABLE_ORDERS + "(" +
                KEY_ID_ORDER + " INTEGER PRIMARY KEY autoincrement," +
                KEY_ORDENID + "," +
                KEY_IDTIPOSERVICIO + "," +
                KEY_TIPOSERVICIO + "," +
                KEY_INDEX + "," +
                KEY_SERIE + "," +
                KEY_FOLIO + "," +
                KEY_NOORDEN + "," +
                KEY_FECHAORDEN + "," +
                KEY_ID_CE + "," +
                KEY_CATEGORIA + "," +
                KEY_IDCONTACTO + "," +
                KEY_FECHAINICIO + "," +
                KEY_FECHAFIN + "," +
                KEY_FALLAREPORTADA + "," +
                KEY_NOMBREUSUARIO + "," +
                KEY_NOMBRESUPERVISOR + "," +
                KEY_ANEXO + "," +
                KEY_FUNCIONANDO + "," +
                KEY_OBSERVACIONES + "," +
                KEY_STATUS + "," +
                KEY_FECHACIERRE + "," +
                KEY_FECHAALTA + "," +
                KEY_USRALTA + "," +
                KEY_USRMOD + "," +
                KEY_SKU + "," +
                KEY_SKUFABRICANTE + "," +
                KEY_MODELO + "," +
                KEY_INTEGREADO + "," +
                KEY_SKU_PADRE + "," +
                KEY_SKUFABRICANTE_PADRE + "," +
                KEY_MODELO_PADRE + "," +
                " UNIQUE (" + KEY_NOORDEN +"));";
        db.execSQL(CREATE_ORDERS_TABLE);

        String CREATE_ROUTINES_TABLE = "CREATE TABLE " + TABLE_ROUTINES + "(" +
                KEY_ID_ROUTINE + " INTEGER PRIMARY KEY autoincrement," +
                KEY_ROUTINEID + "," +
                KEY_TIPOSERVICIOID + "," +
                KEY_MODEL + "," +
                KEY_ORDEN + "," +
                KEY_RUTINA + "," +
                KEY_REVISAR + "," +
                KEY_COLOR + ")";
        db.execSQL(CREATE_ROUTINES_TABLE);

        String CREATE_HAVEROUTINE_TABLE = "CREATE TABLE " + TABLE_HAVEROUTINE + "(" +
                KEY_ID_HAVEROUTINE + " INTEGER PRIMARY KEY autoincrement," +
                KEY_HRTIPOSERVICIOID + "," +
                KEY_HRTIPOSERVICIO + "," +
                KEY_HRRUTINA + ")";
        db.execSQL(CREATE_HAVEROUTINE_TABLE);

        String CREATE_REPAIRS_TABLE = "CREATE TABLE " + TABLE_REPAIRS + "(" +
                KEY_ID_REPAIR + " INTEGER PRIMARY KEY autoincrement," +
                KEY_REPAIRID + "," +
                KEY_ORDERID + "," +
                KEY_CANTIDAD + "," +
                KEY_SKU_R + "," +
                KEY_UNITARIO + "," +
                KEY_TOTAL + "," +
                KEY_ACTIVO + "," +
                KEY_FECHA_MOD + ")";
        db.execSQL(CREATE_REPAIRS_TABLE);

        String CREATE_REQUESTS_TRAVEL_TABLE = "CREATE TABLE " + TABLE_REQUESTS_TRAVEL + "(" +
                KEY_ID_REQUESTS_TRAVEL + " INTEGER PRIMARY KEY autoincrement," +
                KEY_REQUESTSTRAVELID + "," +
                KEY_FOLIORT + "," +
                KEY_FECHASOLICITUD + "," +
                KEY_CUENTA + "," +
                KEY_ID_ZONA + "," +
                KEY_ZONA + "," +
                KEY_FECHASALIDA + "," +
                KEY_FECHAREGRESO + "," +
                KEY_NONOCHES + "," +
                KEY_NOPERSONAS + "," +
                KEY_TIPOTRANSPORTE + "," +
                KEY_ANTICIPO + "," +
                KEY_GASTOSAVION + "," +
                KEY_GASTOSCOMPROBADOS + "," +
                KEY_OBSERVACIONESRT + "," +
                KEY_STATUSRT + "," +
                KEY_FECHAMOD + "," +
                KEY_USRMODRT + "," +
                KEY_IPMOD + ");" ;
        db.execSQL(CREATE_REQUESTS_TRAVEL_TABLE);

        String CREATE_REQUESTS_TRAVEL_DETAILS_TABLE = "CREATE TABLE " + TABLE_REQUESTS_TRAVEL_DETAILS + "(" +
                KEY_ID_REQUESTS_TRAVEL_DETAILS + " INTEGER PRIMARY KEY autoincrement," +
                KEY_REQUESTSTRAVELDETAILSDID + "," +
                KEY_REQUESTSTRAVELDETAILSID + "," +
                KEY_TIPOVIATICO + "," +
                KEY_UNITARIOD  + "," +
                KEY_TOTALD + "," +
                KEY_OBSERVACIONESRTD + ");" ;
        db.execSQL(CREATE_REQUESTS_TRAVEL_DETAILS_TABLE);

        String CREATE_REQUESTS_TRAVEL_TICKETS_TABLE = "CREATE TABLE " + TABLE_REQUESTS_TRAVEL_TICKETS + "(" +
                KEY_ID_REQUESTS_TRAVEL_TICKETS + " INTEGER PRIMARY KEY autoincrement," +
                KEY_REQUESTSTRAVELTICKETSSBAID + "," +
                KEY_REQUESTSTRAVELTICKETSVID + "," +
                KEY_ID_AO + "," +
                KEY_AEROORIGEN + "," +
                KEY_ID_AD + "," +
                KEY_AERODESTINO + "," +
                KEY_FECHA_SALIDA  + "," +
                KEY_HORA_SALIDA + "," +
                KEY_CLAVE_RESERVACION + "," +
                KEY_COSTO + ");" ;
        db.execSQL(CREATE_REQUESTS_TRAVEL_TICKETS_TABLE);

        String CREATE_REQUESTS_TRAVEL_VOUCHERS_TABLE = "CREATE TABLE " + TABLE_REQUESTS_TRAVEL_VOUCHERS + "(" +
                KEY_ID_REQUESTS_TRAVEL_VOUCHERS + " INTEGER PRIMARY KEY autoincrement," +
                KEY_REQUESTSTRAVELVOUCHERSID + "," +
                KEY_REQUESTSTRAVELVOUCHERSSVID + "," +
                KEY_ID_TIPO_VIATICO + "," +
                KEY_TIPO_VIATICO + "," +
                KEY_ID_TIPO_COMPROBANTE + "," +
                KEY_TIPO_COMPROBANTE + "," +
                KEY_SERIE_V + "," +
                KEY_FOLIO_V + "," +
                KEY_FECHA_V + "," +
                KEY_RFC_PROVEEDOR + "," +
                KEY_MONEDA + "," +
                KEY_TOTAL_V + "," +
                KEY_XML + "," +
                KEY_PDF + "," +
                KEY_STATUS_V + ");" ;
        db.execSQL(CREATE_REQUESTS_TRAVEL_VOUCHERS_TABLE);

        // Cat Last Record Request Travel
        String CREATE_LR_RT_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_LR_RT + "(" +
                KEY_ID_LR_RT + " INTEGER PRIMARY KEY autoincrement," +
                KEY_LR_RTID + "," +
                KEY_LR_RT_FOLIO + ")";
        db.execSQL(CREATE_LR_RT_TABLE);

        // Catalogs
        // Cat Counts
        String CREATE_COUNTS_TABLE = "CREATE TABLE " + TABLE_COUNTS + "(" +
                KEY_ID_COUNT + " INTEGER PRIMARY KEY autoincrement," +
                KEY_COUNTID + "," +
                KEY_COUNT + ")";
        db.execSQL(CREATE_COUNTS_TABLE);

        // Cat Zones
        String CREATE_ZONES_TABLE = "CREATE TABLE " + TABLE_ZONES + "(" +
                KEY_ID_ZONE + " INTEGER PRIMARY KEY autoincrement," +
                KEY_ZONEID + "," +
                KEY_ZONE + ")";
        db.execSQL(CREATE_ZONES_TABLE);

        // Cat Transports
        String CREATE_TRANSPORTS_TABLE = "CREATE TABLE " + TABLE_TRANSPORTS + "(" +
                KEY_ID_TRANSPORT + " INTEGER PRIMARY KEY autoincrement," +
                KEY_TRANSPORTID + "," +
                KEY_TRANSPORT + ")";
        db.execSQL(CREATE_TRANSPORTS_TABLE);

        // Cat Travels
        String CREATE_TRAVELS_TABLE = "CREATE TABLE " + TABLE_TRAVELS + "(" +
                KEY_ID_TRAVEL + " INTEGER PRIMARY KEY autoincrement," +
                KEY_TRAVELID + "," +
                KEY_TRAVEL + ")";
        db.execSQL(CREATE_TRAVELS_TABLE);

        // Cat Airports
        String CREATE_AIRPORTS_TABLE = "CREATE TABLE " + TABLE_AIRPORTS + "(" +
                KEY_ID_AIRPORTS + " INTEGER PRIMARY KEY autoincrement," +
                KEY_AIRPORTID + "," +
                KEY_AIRPORT + ")";
        db.execSQL(CREATE_AIRPORTS_TABLE);

        // Cat Vouchers
        String CREATE_VOUCHERS_TABLE = "CREATE TABLE " + TABLE_VOUCHERS + "(" +
                KEY_ID_VOUCHERS + " INTEGER PRIMARY KEY autoincrement," +
                KEY_VOUCHERID + "," +
                KEY_VOUCHER + ")";
        db.execSQL(CREATE_VOUCHERS_TABLE);

        // Cat Providers
        String CREATE_PROVIDERS_TABLE = "CREATE TABLE " + TABLE_PROVIDERS + "(" +
                KEY_ID_PROVIDERS + " INTEGER PRIMARY KEY autoincrement," +
                KEY_PROVIDERID + "," +
                KEY_RFC_PROVIDER + "," +
                KEY_NAME_PROVIDER + ")";
        db.execSQL(CREATE_PROVIDERS_TABLE);

        // Cat SKU Repairs
        String CREATE_SKU_REPAIRS_TABLE = "CREATE TABLE " + TABLE_SKU_REPAIRS + "(" +
                KEY_ID_SKU_REPAIRS + " INTEGER PRIMARY KEY autoincrement," +
                KEY_SKU_REPAIR + "," +
                KEY_DESC_SKU_REPAIRS + ")";
        db.execSQL(CREATE_SKU_REPAIRS_TABLE);

        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROUTINES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HAVEROUTINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REPAIRS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REQUESTS_TRAVEL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REQUESTS_TRAVEL_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REQUESTS_TRAVEL_TICKETS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REQUESTS_TRAVEL_VOUCHERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COUNTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ZONES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSPORTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAVELS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AIRPORTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VOUCHERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROVIDERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SKU_REPAIRS);
        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public void addUser(String foto, String name, String email, String uid, String created_at) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FOTO, foto);
        values.put(KEY_NAME, name); // Name
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_UID, uid); // Uid
        values.put(KEY_CREATED_AT, created_at); // Created At

        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    /**
     * Storing routines details in database
     * */
    public void addRoutines(String routineid, String tiposervicioid, String modelo, String orden, String rutina, String revisar, String color) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ROUTINEID, routineid);
        values.put(KEY_TIPOSERVICIOID, tiposervicioid);
        values.put(KEY_MODEL, modelo);
        values.put(KEY_ORDEN, orden);
        values.put(KEY_RUTINA, rutina);
        values.put(KEY_REVISAR, revisar);
        values.put(KEY_COLOR, color);

        // Inserting Row
        long id = db.insert(TABLE_ROUTINES, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New routines inserted into sqlite: " + id);
    }

    /**
     * Storing routines details in database
     * */
    public void addSKURepairs(String sku, String descripcion) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SKU_REPAIR, sku);
        values.put(KEY_DESC_SKU_REPAIRS, descripcion);

        // Inserting Row
        long id = db.insert(TABLE_SKU_REPAIRS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New routines inserted into sqlite: " + id);
    }

    /**
     * Storing have routine?
     * */
    public void addHaveRoutine(String tiposervicioid, String tiposervicio, String rutina) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_HRTIPOSERVICIOID, tiposervicioid);
        values.put(KEY_HRTIPOSERVICIO, tiposervicio);
        values.put(KEY_HRRUTINA, rutina);

        // Inserting Row
        long id = db.insert(TABLE_HAVEROUTINE, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New have routine? inserted into sqlite: " + id);
    }

    /**
     * Storing repairs details in database
     * */
    public void addRepairs(String id_or, String id_orden_js, String cantidad, String sku_js, String unitario, String total, String activo, String fecha_mod) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_REPAIRID, id_or);
        values.put(KEY_ORDERID, id_orden_js);
        values.put(KEY_CANTIDAD, cantidad);
        values.put(KEY_SKU_R, sku_js);
        values.put(KEY_UNITARIO, unitario);
        values.put(KEY_TOTAL, total);
        values.put(KEY_ACTIVO, activo);
        values.put(KEY_FECHA_MOD, fecha_mod);

        // Inserting Row
        long id = db.insert(TABLE_REPAIRS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New repairs inserted into sqlite: " + id);
    }

    /**
     * Storing orders details in database
     * */
    public void addOrdesByUser(String ordenid, String tiposervicioid, String tiposervicio, String index_orden, String serie, String folio,
                               String no_orden, String fecha_orden, String id_ce, String categoria, String id_contacto, String fecha_inicio,
                               String fecha_fin, String falla_reportada, String nombre_usuario, String nombre_supervisor, String anexo,
                               String funcionando, String observaciones, String status, String fecha_cierre, String fecha_alta, String usr_alta, String usr_mod, String sku, String sku_fabricante, String modelo,
                               String integrado, String sku_padre, String sku_fabricante_padre, String modelo_padre) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_ORDENID, ordenid);
        values.put(KEY_IDTIPOSERVICIO, tiposervicioid);
        values.put(KEY_TIPOSERVICIO, tiposervicio);
        values.put(KEY_INDEX, index_orden);
        values.put(KEY_SERIE, serie);
        values.put(KEY_FOLIO, folio);
        values.put(KEY_NOORDEN, no_orden);
        values.put(KEY_FECHAORDEN, fecha_orden);
        values.put(KEY_ID_CE, id_ce);
        values.put(KEY_CATEGORIA, categoria);
        values.put(KEY_IDCONTACTO, id_contacto);
        values.put(KEY_FECHAINICIO, fecha_inicio);
        values.put(KEY_FECHAFIN, fecha_fin);
        values.put(KEY_FALLAREPORTADA, falla_reportada);
        values.put(KEY_NOMBREUSUARIO, nombre_usuario);
        values.put(KEY_NOMBRESUPERVISOR, nombre_supervisor);
        values.put(KEY_ANEXO, anexo);
        values.put(KEY_FUNCIONANDO, funcionando);
        values.put(KEY_OBSERVACIONES, observaciones);
        values.put(KEY_STATUS, status);
        values.put(KEY_FECHACIERRE, fecha_cierre);
        values.put(KEY_FECHAALTA, fecha_alta);
        values.put(KEY_USRALTA, usr_alta);
        values.put(KEY_USRMOD, usr_mod);
        values.put(KEY_SKU, sku);
        values.put(KEY_SKUFABRICANTE, sku_fabricante);
        values.put(KEY_MODELO, modelo);
        values.put(KEY_INTEGREADO, integrado);
        values.put(KEY_SKU_PADRE, sku_padre);
        values.put(KEY_SKUFABRICANTE_PADRE, sku_fabricante_padre);
        values.put(KEY_MODELO_PADRE, modelo_padre);

        // Inserting Row
        long id = db.insert(TABLE_ORDERS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "NEW ORDER (ANTES JOY DIVISION) inserted into sqlite: " + id);
    }

    /**
     * Storing requests travel details in database
     * */
    public void addRequestsTravelByST(String svid, String folio, String fecha_solicitud, String cuenta, String id_zona, String zona,
                                      String fecha_salida, String fecha_regreso, String no_noches, String no_personas, String tipo_transporte,
                                      String anticipo, String gastos_avion, String gastos_comprobados, String observaciones, String status,
                                      String fecha_mod, String usr_mod, String ip_mod) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_REQUESTSTRAVELID, svid);
        values.put(KEY_FOLIORT, folio);
        values.put(KEY_FECHASOLICITUD, fecha_solicitud);
        values.put(KEY_CUENTA, cuenta);
        values.put(KEY_ID_ZONA, id_zona);
        values.put(KEY_ZONA, zona);
        values.put(KEY_FECHASALIDA, fecha_salida);
        values.put(KEY_FECHAREGRESO, fecha_regreso);
        values.put(KEY_NONOCHES, no_noches);
        values.put(KEY_NOPERSONAS, no_personas);
        values.put(KEY_TIPOTRANSPORTE, tipo_transporte);
        values.put(KEY_ANTICIPO, anticipo);
        values.put(KEY_GASTOSAVION, gastos_avion);
        values.put(KEY_GASTOSCOMPROBADOS, gastos_comprobados);
        values.put(KEY_OBSERVACIONESRT, observaciones);
        values.put(KEY_STATUSRT, status);
        values.put(KEY_FECHAMOD, fecha_mod);
        values.put(KEY_USRMODRT, usr_mod);
        values.put(KEY_IPMOD, ip_mod);

        // Inserting Row
        long id = db.insert(TABLE_REQUESTS_TRAVEL, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "NEW REQUESTS TRAVEL inserted into sqlite: " + id);
    }

    /**
     * Storing requests travel details in database
     * */
    public void addRequestsTravelDetailsBySVID(String id_svd, String id_sv, String tipo_viatico, String unitario, String total,
                                               String observaciones) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_REQUESTSTRAVELDETAILSDID, id_svd);
        values.put(KEY_REQUESTSTRAVELDETAILSID, id_sv);
        values.put(KEY_TIPOVIATICO, tipo_viatico);
        values.put(KEY_UNITARIOD, unitario);
        values.put(KEY_TOTALD, total);
        values.put(KEY_OBSERVACIONESRTD, observaciones);

        // Inserting Row
        long id = db.insert(TABLE_REQUESTS_TRAVEL_DETAILS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "NEW REQUESTS TRAVEL DETAILS inserted into sqlite: " + id);
    }

    /**
     * Storing requests travel tickets in database
     * */
    public void addRequestsTravelTicketsBySVID(String id_sba, String id_sv, String id_ao, String aero_origen, String id_ad,
                                               String aero_destino, String fecha_salida, String hora_salida, String clave_reservacion, String costo) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_REQUESTSTRAVELTICKETSSBAID, id_sba);
        values.put(KEY_REQUESTSTRAVELTICKETSVID, id_sv);
        values.put(KEY_ID_AO, id_ao);
        values.put(KEY_AEROORIGEN, aero_origen);
        values.put(KEY_ID_AD, id_ad);
        values.put(KEY_AERODESTINO, aero_destino);
        values.put(KEY_FECHA_SALIDA, fecha_salida);
        values.put(KEY_HORA_SALIDA, hora_salida);
        values.put(KEY_CLAVE_RESERVACION, clave_reservacion);
        values.put(KEY_COSTO, costo);

        // Inserting Row
        long id = db.insert(TABLE_REQUESTS_TRAVEL_TICKETS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "NEW REQUESTS TRAVEL TICKETS inserted into sqlite: " + id);
    }

    /**
     * Storing requests travel vouchers in database
     * */
    public void addRequestsTravelVouchersBySVID(String id_comprobante, String id_sv, String id_tipoViatico, String tipo_viatico, String id_tipoComprobante,
                                                String tipo_comprobante, String serie, String folio, String fecha, String rfc_proveedor,
                                                String moneda, String total, String xml_v, String pdf_v, String status) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_REQUESTSTRAVELVOUCHERSID, id_comprobante);
        values.put(KEY_REQUESTSTRAVELVOUCHERSSVID, id_sv);
        values.put(KEY_ID_TIPO_VIATICO, id_tipoViatico);
        values.put(KEY_TIPOVIATICO, tipo_viatico);
        values.put(KEY_ID_TIPO_COMPROBANTE, id_tipoComprobante);
        values.put(KEY_TIPO_COMPROBANTE, tipo_comprobante);
        values.put(KEY_SERIE_V, serie);
        values.put(KEY_FOLIO_V, folio);
        values.put(KEY_FECHA_V, fecha);
        values.put(KEY_RFC_PROVEEDOR, rfc_proveedor);
        values.put(KEY_MONEDA, moneda);
        values.put(KEY_TOTAL_V, total);
        values.put(KEY_XML, xml_v);
        values.put(KEY_PDF, pdf_v);
        values.put(KEY_STATUS_V, status);

        // Inserting Row
        long id = db.insert(TABLE_REQUESTS_TRAVEL_VOUCHERS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "NEW REQUESTS TRAVEL VOUCHERS inserted into sqlite: " + id);
    }

    /**
     * Storing last record from Solicitudes Viaticos table
     * */
    public void addLastRecordRequestTravel(String sv_id, String folio) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LR_RTID, sv_id);
        values.put(KEY_LR_RT_FOLIO, folio);

        // Inserting Row
        long id = db.insert(TABLE_LR_RT, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New last record request travel inserted into sqlite: " + id);
    }

    /**
     *  Catalogs
     */
    /**
     * Storing counts details in database
     * */
    public void addCatCounts(String cuentaid, String cuenta) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_COUNTID, cuentaid);
        values.put(KEY_COUNT, cuenta);

        // Inserting Row
        long id = db.insert(TABLE_COUNTS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New count inserted into sqlite: " + id);
    }

    /**
     * Storing zones details in database
     * */
    public void addCatZones(String zonaid, String zona) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ZONEID, zonaid);
        values.put(KEY_ZONE, zona);

        // Inserting Row
        long id = db.insert(TABLE_ZONES, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New zone inserted into sqlite: " + id);
    }

    /**
     * Storing transports details in database
     * */
    public void addCatTransports(String transporteid, String transporte) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TRANSPORTID, transporteid);
        values.put(KEY_TRANSPORT, transporte);

        // Inserting Row
        long id = db.insert(TABLE_TRANSPORTS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New transport inserted into sqlite: " + id);
    }

    /**
     * Storing travels details in database
     * */
    public void addCatTravels(String viaticoid, String viatico) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TRAVELID, viaticoid);
        values.put(KEY_TRAVEL, viatico);

        // Inserting Row
        long id = db.insert(TABLE_TRAVELS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New kind of travel inserted into sqlite: " + id);
    }

    /**
     * Storing vouchers in database
     * */
    public void addCatVouchers(String comprobantetipoid, String tipo_comprobante) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_VOUCHERID, comprobantetipoid);
        values.put(KEY_VOUCHER, tipo_comprobante);

        // Inserting Row
        long id = db.insert(TABLE_VOUCHERS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New vouchers inserted into sqlite: " + id);
    }

    /**
     * Storing airports in database
     * */
    public void addCatAirports(String aeropuertoid, String aeropuerto) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_AIRPORTID, aeropuertoid);
        values.put(KEY_AIRPORT, aeropuerto);

        // Inserting Row
        long id = db.insert(TABLE_AIRPORTS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New airports inserted into sqlite: " + id);
    }

    /**
     * Storing providers in database
     * */
    public void addProviders(String contribuyenteid, String rfc, String nombre_contribuyente) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PROVIDERID, contribuyenteid);
        values.put(KEY_RFC_PROVIDER, rfc);
        values.put(KEY_NAME_PROVIDER, nombre_contribuyente);

        // Inserting Row
        long id = db.insert(TABLE_PROVIDERS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New providers inserted into sqlite: " + id);
    }

    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("foto", cursor.getString(1));
            user.put("name", cursor.getString(2));
            user.put("email", cursor.getString(3));
            user.put("uid", cursor.getString(4));
            user.put("created_at", cursor.getString(5));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    /**
     * Getting routines data from database
     * */
    public HashMap<String, String> getRoutines() {
        HashMap<String, String> routine = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_ROUTINES;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            routine.put("rutina_id", cursor.getString(1));
            routine.put("tiposervicio_id", cursor.getString(2));
            routine.put("model", cursor.getString(3));
            routine.put("marca", cursor.getString(4));
            routine.put("equipo", cursor.getString(5));
            routine.put("rutina", cursor.getString(6));
            routine.put("revisar", cursor.getString(7));
            routine.put("color", cursor.getString(8));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching routine from Sqlite: " + routine.toString());

        return routine;
    }

    /**
     * Getting have routine? data from database
     * */
    public HashMap<String, String> getHaveRoutine() {
        HashMap<String, String> haveroutine = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_HAVEROUTINE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            haveroutine.put("tiposervicio_id", cursor.getString(1));
            haveroutine.put("tiposervicio", cursor.getString(2));
            haveroutine.put("rutina", cursor.getString(3));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching routine from Sqlite: " + haveroutine.toString());

        return haveroutine;
    }

    /**
     * Getting orders data from database
     * */
    public HashMap<String, String> getOrdersDetails() {
        HashMap<String, String> orders = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_ORDERS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            orders.put("id_orden", cursor.getString(1));
            orders.put("id_tiposervicio", cursor.getString(2));
            orders.put("tipo_servicio", cursor.getString(3));
            orders.put("index_orden", cursor.getString(4));
            orders.put("serie", cursor.getString(5));
            orders.put("folio", cursor.getString(6));
            orders.put("no_orden", cursor.getString(7));
            orders.put("fecha_orden", cursor.getString(8));
            orders.put("id_ce", cursor.getString(9));
            orders.put("categoria", cursor.getString(10));
            orders.put("id_contacto", cursor.getString(11));
            orders.put("fecha_inicio", cursor.getString(12));
            orders.put("fecha_fin", cursor.getString(13));
            orders.put("falla_reportada", cursor.getString(14));
            orders.put("nombre_usuario", cursor.getString(15));
            orders.put("nombre_supervisor", cursor.getString(16));
            orders.put("anexo", cursor.getString(17));
            orders.put("funcionando", cursor.getString(18));
            orders.put("observaciones", cursor.getString(19));
            orders.put("status", cursor.getString(20));
            orders.put("fecha_cierre", cursor.getString(21));
            orders.put("fecha_alta", cursor.getString(22));
            orders.put("usr_alta", cursor.getString(23));
            orders.put("usr_mod", cursor.getString(24));
            orders.put("sku", cursor.getString(25));
            orders.put("sku_fabricante", cursor.getString(26));
            orders.put("modelo", cursor.getString(27));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching orders from Sqlite: " + orders.toString());

        return orders;
    }

    /**
     * Getting last record request travel data from database
     * */
    public HashMap<String, String> getLastFolio() {
        HashMap<String, String> lastFolio = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_LR_RT;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            lastFolio.put("lr_rtid", cursor.getString(1));
            lastFolio.put("lr_rt_folio", cursor.getString(2));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching last record request travel from Sqlite: " + lastFolio.toString());

        return lastFolio;
    }

    //Catalogs
    /**
     * Getting counts data from database
     * */
    public HashMap<String, String> getCounts() {
        HashMap<String, String> counts = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_COUNTS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            counts.put("cuenta_id", cursor.getString(1));
            counts.put("cuenta", cursor.getString(2));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching counts from Sqlite: " + counts.toString());

        return counts;
    }

    /**
     * Getting zones data from database
     * */
    public HashMap<String, String> getZones() {
        HashMap<String, String> zones = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_ZONES;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            zones.put("zona_id", cursor.getString(1));
            zones.put("zona", cursor.getString(2));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching zones from Sqlite: " + zones.toString());

        return zones;
    }

    /**
     * Getting transports data from database
     * */
    public HashMap<String, String> getTransports() {
        HashMap<String, String> transports = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_TRANSPORTS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            transports.put("transporte_id", cursor.getString(1));
            transports.put("transporte", cursor.getString(2));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching transports from Sqlite: " + transports.toString());

        return transports;
    }

    /**
     * Getting orders data from database
     * */
    public Cursor getOrdersListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_ORDERS, new String[] {KEY_ID_ORDER,
                        KEY_IDTIPOSERVICIO, KEY_NOORDEN, KEY_TIPOSERVICIO, KEY_FECHAORDEN, KEY_STATUS},
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting routines data from database
     * */
    public Cursor getRoutinesListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_ROUTINES,
                new String[] {
                        KEY_ID_ROUTINE,
                        KEY_ROUTINEID,
                        KEY_TIPOSERVICIOID,
                        KEY_MODEL,
                        KEY_ORDEN,
                        KEY_RUTINA,
                        KEY_REVISAR,
                        KEY_COLOR},
                null, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting repairs data from database
     * */
    public Cursor getRepairsListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_REPAIRS,
                new String[] {KEY_ID_REPAIR,
                        KEY_REPAIRID,
                        KEY_ORDERID,
                        KEY_CANTIDAD,
                        KEY_SKU_R,
                        KEY_UNITARIO,
                        KEY_TOTAL,
                        KEY_ACTIVO,
                        KEY_FECHA_MOD},
                null, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting reapirs data by order id from database
     * */
    public Cursor getRequestsTravelListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_REQUESTS_TRAVEL, new String[] {
                        KEY_ID_REQUESTS_TRAVEL,
                        KEY_FOLIORT,
                        KEY_FECHASOLICITUD,
                        KEY_ID_ZONA,
                        KEY_ZONA,
                        KEY_FECHASALIDA,
                        KEY_FECHAREGRESO,
                        KEY_GASTOSCOMPROBADOS,
                        KEY_STATUSRT
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting requests travel details data by order id from database
     * */
    public Cursor getRequestsTravelDetailsListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_REQUESTS_TRAVEL_DETAILS, new String[]{
                        KEY_ID_REQUESTS_TRAVEL_DETAILS,
                        KEY_REQUESTSTRAVELDETAILSDID,
                        KEY_REQUESTSTRAVELDETAILSID,
                        KEY_TIPOVIATICO,
                        KEY_UNITARIOD,
                        KEY_TOTALD,
                        KEY_OBSERVACIONESRTD
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting requests travel tickets data by order id from database
     * */
    public Cursor getRequestsTravelTicektsListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_REQUESTS_TRAVEL_TICKETS, new String[]{
                        KEY_ID_REQUESTS_TRAVEL_TICKETS,
                        KEY_REQUESTSTRAVELTICKETSSBAID,
                        KEY_REQUESTSTRAVELTICKETSVID,
                        KEY_ID_AO,
                        KEY_AEROORIGEN,
                        KEY_ID_AD,
                        KEY_AERODESTINO,
                        KEY_FECHA_SALIDA,
                        KEY_HORA_SALIDA,
                        KEY_CLAVE_RESERVACION,
                        KEY_COSTO
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting requests travel vouchers data by order id from database
     * */
    public Cursor getRequestsTravelVouchersListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_REQUESTS_TRAVEL_VOUCHERS, new String[]{
                        KEY_ID_REQUESTS_TRAVEL_VOUCHERS,
                        KEY_REQUESTSTRAVELVOUCHERSID,
                        KEY_REQUESTSTRAVELVOUCHERSSVID,
                        KEY_ID_TIPO_VIATICO,
                        KEY_TIPO_VIATICO,
                        KEY_ID_TIPO_COMPROBANTE,
                        KEY_TIPO_COMPROBANTE,
                        KEY_SERIE_V,
                        KEY_FOLIO_V,
                        KEY_FECHA_V,
                        KEY_RFC_PROVEEDOR,
                        KEY_MONEDA,
                        KEY_TOTAL_V,
                        KEY_XML,
                        KEY_PDF,
                        KEY_STATUS_V
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting order by no. order from database
     */
    public Cursor fetchOrderByNumber(String inputText) throws SQLException {
        SQLiteDatabase db = this.getReadableDatabase();
        Log.w(TAG, inputText);
        Cursor mCursor = null;
        if (inputText == null  ||  inputText.length () == 0)  {
            mCursor = db.query(TABLE_ORDERS, new String[] {KEY_ID_ORDER,
                            KEY_NOORDEN, KEY_TIPOSERVICIO, KEY_FECHAORDEN, KEY_STATUS},
                    null, null, null, null, null);

        }
        else {
            mCursor = db.query(true, TABLE_ORDERS, new String[] {KEY_ID_ORDER,
                            KEY_NOORDEN, KEY_TIPOSERVICIO, KEY_FECHAORDEN, KEY_STATUS},
                    KEY_NOORDEN + " like '%" + inputText + "%'", null,
                    null, null, null, null);
        }
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    //Catalogs
    /**
     * Getting counts data from database
     * */
    public Cursor getCountsListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_COUNTS, new String[] {
                        KEY_ID_COUNT,
                        KEY_COUNTID,
                        KEY_COUNT
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting zones data from database
     * */
    public Cursor getZonesListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_ZONES, new String[] {
                        KEY_ID_ZONE,
                        KEY_ZONEID,
                        KEY_ZONE
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting transports data from database
     * */
    public Cursor getTransportsListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_TRANSPORTS, new String[] {
                        KEY_ID_TRANSPORT,
                        KEY_TRANSPORTID,
                        KEY_TRANSPORT
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting travels data from database
     * */
    public Cursor getTravelsListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_TRAVELS, new String[] {
                        KEY_ID_TRAVEL,
                        KEY_TRAVELID,
                        KEY_TRAVEL
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting airports data from database
     * */
    public Cursor getAirportsListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_AIRPORTS, new String[] {
                        KEY_ID_AIRPORTS,
                        KEY_AIRPORTID,
                        KEY_AIRPORT
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting vouchers data from database
     * */
    public Cursor getVouchersListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_VOUCHERS, new String[] {
                        KEY_ID_VOUCHERS,
                        KEY_VOUCHERID,
                        KEY_VOUCHER
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting providers data from database
     * */
    public Cursor getProvidersListCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(TABLE_PROVIDERS, new String[] {
                        KEY_ID_PROVIDERS,
                        KEY_PROVIDERID,
                        KEY_RFC_PROVIDER,
                        KEY_NAME_PROVIDER
                },
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting order data by order number from database. To details
     */
    public HashMap<String, String> getOrderDetails(String noorder) {
        HashMap<String, String> order = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_ORDERS + " WHERE " + KEY_NOORDEN + " LIKE '%" + noorder + "%'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            order.put("id_orden", cursor.getString(1));
            order.put("id_tiposervicio", cursor.getString(2));
            order.put("tipo_servicio", cursor.getString(3));
            order.put("index_orden", cursor.getString(4));
            order.put("serie", cursor.getString(5));
            order.put("folio", cursor.getString(6));
            order.put("no_orden", cursor.getString(7));
            order.put("fecha_orden", cursor.getString(8));
            order.put("id_ce", cursor.getString(9));
            order.put("categoria", cursor.getString(10));
            order.put("id_contacto", cursor.getString(11));
            order.put("fecha_inicio", cursor.getString(12));
            order.put("fecha_fin", cursor.getString(13));
            order.put("falla_reportada", cursor.getString(14));
            order.put("nombre_usuario", cursor.getString(15));
            order.put("nombre_supervisor", cursor.getString(16));
            order.put("anexo", cursor.getString(17));
            order.put("funcionando", cursor.getString(18));
            order.put("observaciones", cursor.getString(19));
            order.put("status", cursor.getString(20));
            order.put("fecha_cierre", cursor.getString(21));
            order.put("fecha_alta", cursor.getString(22));
            order.put("usr_alta", cursor.getString(23));
            order.put("usr_mod", cursor.getString(24));
            order.put("sku" , cursor.getString(25));
            order.put("sku_fabricante" , cursor.getString(26));
            order.put("modelo" , cursor.getString(27));
            order.put("integrado", cursor.getString(28));
            order.put("sku_padre", cursor.getString(29));
            order.put("sku_fabricante_padre", cursor.getString(30));
            order.put("modelo_padre", cursor.getString(31));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching order by number from Sqlite: " + order.toString());

        return order;
    }

    /**
     * Getting order data by order number from database. To details
     */
    public HashMap<String, String> getRequestsTravelDetails(String foliort) {
        HashMap<String, String> requestTravel = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_REQUESTS_TRAVEL + " WHERE " + KEY_FOLIORT + " LIKE '%" + foliort + "%'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            requestTravel.put("svid", cursor.getString(1));
            requestTravel.put("foliort", cursor.getString(2));
            requestTravel.put("fecha_solicitud", cursor.getString(3));
            requestTravel.put("cuenta", cursor.getString(4));
            requestTravel.put("id_zona", cursor.getString(5));
            requestTravel.put("zona", cursor.getString(6));
            requestTravel.put("fecha_salida", cursor.getString(7));
            requestTravel.put("fecha_regreso", cursor.getString(8));
            requestTravel.put("no_noches", cursor.getString(9));
            requestTravel.put("no_personas", cursor.getString(10));
            requestTravel.put("tipo_transporte", cursor.getString(11));
            requestTravel.put("anticipo", cursor.getString(12));
            requestTravel.put("gastos_avion", cursor.getString(13));
            requestTravel.put("gastos_comprobados", cursor.getString(14));
            requestTravel.put("observacionesrt", cursor.getString(15));
            requestTravel.put("statustr", cursor.getString(16));
            requestTravel.put("fecha_mod", cursor.getString(17));
            requestTravel.put("usr_modrt", cursor.getString(18));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching requests travel by number from Sqlite: " + requestTravel.toString());

        return requestTravel;
    }

    /**
     * Getting repair data by id order repair number from database. To details
     */
    public HashMap<String, String> getRepairDetails(String id_or) {
        HashMap<String, String> requestRepair = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_REPAIRS + " WHERE " + KEY_REPAIRID + " = '" + id_or + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            requestRepair.put("repairid", cursor.getString(1));
            requestRepair.put("orderid", cursor.getString(2));
            requestRepair.put("cantidad", cursor.getString(3));
            requestRepair.put("sku_r", cursor.getString(4));
            requestRepair.put("unitario", cursor.getString(5));
            requestRepair.put("total", cursor.getString(6));
            requestRepair.put("activo", cursor.getString(7));
            requestRepair.put("fecha_mod", cursor.getString(8));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching requests repairs by id_or from Sqlite: " + requestRepair.toString());

        return requestRepair;
    }

    /**
     * Getting requests travel by no. folio from database
     */
    public Cursor fetchRequestsTravelByNumber(String inputText) throws SQLException {
        SQLiteDatabase db = this.getReadableDatabase();
        Log.w(TAG, inputText);
        Cursor mCursor = null;
        if (inputText == null  ||  inputText.length () == 0)  {
            mCursor = db.query(TABLE_REQUESTS_TRAVEL, new String[] {
                            KEY_ID_REQUESTS_TRAVEL,
                            KEY_FOLIORT,
                            KEY_FECHASOLICITUD,
                            KEY_ID_ZONA,
                            KEY_ZONA,
                            KEY_FECHASALIDA,
                            KEY_FECHAREGRESO,
                            KEY_GASTOSCOMPROBADOS,
                            KEY_STATUSRT
                    },
                    null, null, null, null, null);

        }
        else {
            mCursor = db.query(true, TABLE_REQUESTS_TRAVEL, new String[] {
                            KEY_ID_REQUESTS_TRAVEL,
                            KEY_FOLIORT,
                            KEY_FECHASOLICITUD,
                            KEY_ID_ZONA,
                            KEY_ZONA,
                            KEY_FECHASALIDA,
                            KEY_FECHAREGRESO,
                            KEY_GASTOSCOMPROBADOS,
                            KEY_STATUSRT
                    },
                    KEY_FOLIORT + " like '%" + inputText + "%'", null,
                    null, null, null, null);
        }
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Getting sku repairs data from database autocomplete
     * */
    public List<SKURepairs> readSKURepairs(String searchTerm) {

        List<SKURepairs> recordsList = new ArrayList<SKURepairs>();

        // select query
        String sql = "";
        sql += "SELECT * FROM " + TABLE_SKU_REPAIRS;
        sql += " WHERE " + KEY_DESC_SKU_REPAIRS + " LIKE '%" + searchTerm + "%'";
        sql += " ORDER BY " + KEY_SKU_REPAIR + " DESC";
        sql += " LIMIT 0,5";

        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                // int productId = Integer.parseInt(cursor.getString(cursor.getColumnIndex(fieldProductId)));
                String sku = cursor.getString(cursor.getColumnIndex(KEY_SKU_REPAIR));
                String descripcion = cursor.getString(cursor.getColumnIndex(KEY_DESC_SKU_REPAIRS));
                SKURepairs sKURepairs = new SKURepairs(sku, descripcion);

                // add to list
                recordsList.add(sKURepairs);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsList;
    }

    /**
     * Getting sku repairs data from database autocomplete
     * */
    public List<SKURepairs> readSKURepairsEdit(String sku_search) {

        List<SKURepairs> recordsListEdit = new ArrayList<SKURepairs>();

        // select query
        String sql = "";
        sql += "SELECT * FROM " + TABLE_SKU_REPAIRS;
        sql += " WHERE " + KEY_SKU_REPAIR + " LIKE '%" + sku_search + "%'";

        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String sku = cursor.getString(cursor.getColumnIndex(KEY_SKU_REPAIR));
                String descripcion = cursor.getString(cursor.getColumnIndex(KEY_DESC_SKU_REPAIRS));
                SKURepairs sKURepairs = new SKURepairs(sku, descripcion);
                // add to list
                recordsListEdit.add(sKURepairs);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsListEdit;
    }



    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }

    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteOrdersAttended() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_ORDERS, null, null);
        db.close();

        Log.d(TAG, "Deleted all orders info from sqlite");
    }

    /**
     * Re crate database Delete table routine
     * */
    public void deleteRoutines() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_ROUTINES, null, null);
        db.close();

        Log.d(TAG, "Deleted all routines info from sqlite");
    }

    /**
     * Re crate database Delete table routine
     * */
    public void deleteHaveRoutine() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_HAVEROUTINE, null, null);
        db.close();

        Log.d(TAG, "Deleted have routine? info from sqlite");
    }

    /**
     * Re crate database Delete table routine
     * */
    public void deleteRepairs() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_REPAIRS, null, null);
        db.close();

        Log.d(TAG, "Deleted all repairs info from sqlite");
    }

    /**
     * Re crate database Delete table requestsTravel
     * */
    public void deleteRequestsTravel() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_REQUESTS_TRAVEL, null, null);
        db.close();

        Log.d(TAG, "Deleted all requests travel info from sqlite");
    }

    /**
     * Re crate database Delete table requestsTravelDetails
     * */
    public void deleteRequestsTravelDetails() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_REQUESTS_TRAVEL_DETAILS, null, null);
        db.close();

        Log.d(TAG, "Deleted all requests travel details info from sqlite");
    }

    /**
     * Re crate database Delete table requestsTravelTickets
     * */
    public void deleteRequestsTravelTickets() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_REQUESTS_TRAVEL_TICKETS, null, null);
        db.close();

        Log.d(TAG, "Deleted all requests travel tickets info from sqlite");
    }

    /**
     * Re crate database Delete table requestsTravelVouchers
     * */
    public void deleteRequestsTravelVouchers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_REQUESTS_TRAVEL_VOUCHERS, null, null);
        db.close();

        Log.d(TAG, "Deleted all requests travel vouchers info from sqlite");
    }

    /**
     * Delete last record request travel table
     * */
    public void deleteLastRecordRequestTravel() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_LR_RT, null, null);
        db.close();

        Log.d(TAG, "Deleted all last record request travel info from sqlite");
    }

    //Catalogs
    /**
     * Re crate database Delete cat counts
     * */
    public void deleteCounts() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_COUNTS, null, null);
        db.close();

        Log.d(TAG, "Deleted all counts info from sqlite");
    }

    /**
     * Re crate database Delete cat zones
     * */
    public void deleteZones() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_ZONES, null, null);
        db.close();

        Log.d(TAG, "Deleted all zones info from sqlite");
    }

    /**
     * Re crate database Delete cat transports
     * */
    public void deleteTransports() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_TRANSPORTS, null, null);
        db.close();

        Log.d(TAG, "Deleted all transports info from sqlite");
    }

    /**
     * Re crate database Delete cat travels
     * */
    public void deleteTravels() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_TRAVELS, null, null);
        db.close();

        Log.d(TAG, "Deleted all travels sppiner info from sqlite");
    }

    /**
     * Re crate database Delete cat travels
     * */
    public void deleteAirports() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_AIRPORTS, null, null);
        db.close();

        Log.d(TAG, "Deleted all airports sppiner info from sqlite");
    }

    /**
     * Re crate database Delete cat vouchers
     * */
    public void deleteVouchers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_VOUCHERS, null, null);
        db.close();

        Log.d(TAG, "Deleted all vouchers sppiner info from sqlite");
    }

    /**
     * Re crate database Delete cat vouchers
     * */
    public void deleteProviders() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_PROVIDERS, null, null);
        db.close();

        Log.d(TAG, "Deleted all providers sppiner info from sqlite");
    }

    /**
     * Re crate database Delete cat vouchers
     * */
    public void deleteSKURepairs() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_SKU_REPAIRS, null, null);
        db.close();

        Log.d(TAG, "Deleted all sku reapirs info from sqlite");
    }
}