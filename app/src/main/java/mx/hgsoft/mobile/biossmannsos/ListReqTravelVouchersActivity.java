package mx.hgsoft.mobile.biossmannsos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 28/06/16.
 */
public class ListReqTravelVouchersActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();

    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private SessionManager session;

    /* XML elements view */
    private TextView txtFoliort;
    private TextView txtFechaSolicitud;
    private TextView txtZona;
    private TextView txtFechaSalida;
    private TextView txtFechaRegreso;
    private TextView txtAnticipo;
    private TextView txtGastosAvion;
    private TextView txtGastosComprobados;
    private TextView txtStatusrt;

    private String svid = "";
    private String foliort = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_reqtravel_vouchers);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fabOptions = (FloatingActionButton) findViewById(R.id.addVouchersRequestsTravel);
        fabOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRegisterReqTravelVouchers();
            }
        });

        /* Getting order number from List */
        foliort = getIntent().getExtras().getString("foliort");

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Fetching requests travrl details from sqlite
        HashMap<String, String> order = db.getRequestsTravelDetails(foliort);
        svid = order.get("svid");
        String foliort = order.get("foliort");
        String fecha_solicitud = order.get("fecha_solicitud");
        String zona = order.get("zona");
        String fecha_salida = order.get("fecha_salida");
        String fecha_regreso = order.get("fecha_regreso");
        String anticipo = order.get("anticipo");
        String gastos_avion = order.get("gastos_avion");
        String gastos_comprobados = order.get("gastos_comprobados");
        String statustr = order.get("statustr");

        // Setting values from sqlite to view xml
        txtFoliort = (TextView) findViewById(R.id.foliort_view);
        txtFoliort.setText(foliort);
        txtFechaSolicitud = (TextView) findViewById(R.id.fecha_solicitud_view);
        txtFechaSolicitud.setText(fecha_solicitud);
        txtZona = (TextView) findViewById(R.id.zona_view);
        txtZona.setText(zona);
        txtFechaSalida = (TextView) findViewById(R.id.fechasalida_view);
        txtFechaSalida.setText(fecha_salida);
        txtFechaRegreso = (TextView) findViewById(R.id.fecharegreso_view);
        txtFechaRegreso.setText(fecha_regreso);
        txtAnticipo = (TextView) findViewById(R.id.label_anticipo_view);
        txtAnticipo.setText(anticipo);
        txtGastosAvion = (TextView) findViewById(R.id.label_gastosavion_view);
        txtGastosAvion.setText(gastos_avion);
        txtGastosComprobados = (TextView) findViewById(R.id.label_gastoscomprobados_view);
        txtGastosComprobados.setText(gastos_comprobados);
        txtStatusrt = (TextView) findViewById(R.id.label_statusrtrt_view);
        txtStatusrt.setText(statustr);

        if (foliort.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el folio de la solicitud de viáticos!", Toast.LENGTH_LONG)
                    .show();
        } else {
            db.deleteRequestsTravelVouchers();
            getRequestsTravelVouchersBySVID(svid);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), ManageRequestsTravelActivity.class);
        myIntent.putExtra("foliort", foliort);
        db.deleteRequestsTravelVouchers();
        startActivityForResult(myIntent, 0);
        return true;
    }

    /**
     * function to get JSON from API_REST about Requests Travel Vouchers by id SV
     * */
    private void getRequestsTravelVouchersBySVID(final String svid) {
        // Tag used to cancel the request
        String tag_string_reqvouchers = "req_requestsTravelVouchers";

        //pDialog.setMessage("Obteniendo Información...");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REQUESTSTRAVELVOUCHERS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Requests Travel Vouchers response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("requestsTravelVouchers");

                        for(int i=0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id_comprobante = jsonObject.optString("ID_Comprobante").toString();
                            String id_sv = jsonObject.optString("ID_Solicitud").toString();
                            String id_tipoViatico = jsonObject.optString("ID_TipoViatico").toString();
                            String tipo_viatico = jsonObject.optString("Tipo_Viatico").toString();
                            String id_tipoComprobante = jsonObject.optString("ID_TipoComprobante").toString();
                            String tipo_comprobante = jsonObject.optString("Tipo_Comprobante").toString();
                            String serie = jsonObject.optString("Serie").toString();
                            String folio = jsonObject.optString("Folio").toString();
                            String fecha = jsonObject.optString("Fecha").toString();
                            String rfc_proveedor = jsonObject.optString("RFC_Proveedor").toString();
                            String moneda = jsonObject.optString("Moneda").toString();
                            String total = jsonObject.optString("Total").toString();
                            String xml_v = jsonObject.optString("XML").toString();
                            String pdf_v = jsonObject.optString("PDF").toString();
                            String status = jsonObject.optString("Status").toString();


                            // Inserting row in requests travel vouchers table
                            db.addRequestsTravelVouchersBySVID(id_comprobante, id_sv, id_tipoViatico, tipo_viatico, id_tipoComprobante, tipo_comprobante, serie, folio, fecha, rfc_proveedor, moneda, total, xml_v, pdf_v, status);
                        }
                        //Generate ListView from SQLite Database
                        displayListViewVouchers();
                    } else {
                        // Error in orders. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to requests travel url
                Map<String, String> params = new HashMap<String, String>();
                params.put("svid", svid);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_reqvouchers);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void displayListViewVouchers() {
        Cursor cursor = db.getRequestsTravelVouchersListCursor();

        // The desired columns to be bound
        String[] columns = new String[] {
                "tipo_viatico",
                "tipo_comprobante",
                "serie_v",
                "folio_v",
                "fecha_v",
                "rfc_proveedor",
                "total_v",
                "xml_v",
                "pdf_v",
                "status_v"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[] {
                R.id.tipoviatico,
                R.id.tipocomprobante,
                R.id.serie,
                R.id.folio,
                R.id.fecha,
                R.id.rfcproveedor,
                R.id.total_v,
                R.id.xml_v,
                R.id.pdf_v,
                R.id.status_v
        };

        // create the adapter using the cursor pointing to the desired data
        //as well as the layout information
        SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(
                this, R.layout.rows_requests_travel_vouchers,
                cursor,
                columns,
                to,
                0);

        ListView listView = (ListView) findViewById(R.id.listviewrequeststravelvouchers);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);

        /*
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                // Get the cursor, positioned to the corresponding row in the result set
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);

                // Get the folio number from this row in the database.
                String folioNumber = cursor.getString(cursor.getColumnIndexOrThrow("foliort"));
                // Launch List Requests Travel Activity
                Intent intent = new Intent(ListViewRequestsTravel.this,
                        ManageRequestsTravel.class);
                intent.putExtra("foliort", folioNumber);
                startActivity(intent);
            }
        });

        EditText myFilter = (EditText) findViewById(R.id.myFilter);
        myFilter.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                dataAdapter.getFilter().filter(s.toString());
            }
        });

        dataAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence constraint) {
                try {
                    return db.fetchRequestsTravelByNumber(constraint.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });*/

    }

    public void goToRegisterReqTravelVouchers () {
        if (svid.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el id de la solicitud de servicio!", Toast.LENGTH_LONG)
                    .show();
        } else {
            // Launch ManageOrderActivity activity
            Intent intent = new Intent(
                    ListReqTravelVouchersActivity.this,
                    RegisterReqTravelVouchersActivity.class);
            intent.putExtra("foliort", foliort);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        db.deleteRequestsTravelDetails();
        db.deleteTravels();
        db.deleteRequestsTravelVouchers();
        // Launching the login activity
        Intent intent = new Intent(ListReqTravelVouchersActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }
}
