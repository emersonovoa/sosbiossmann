package mx.hgsoft.mobile.biossmannsos;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 14/04/16.
 */
public class ManageRequestsTravelActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();

    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private SessionManager session;

    /* XML elements view */
    private TextView txtFoliort;
    private TextView txtFechaSolicitud;
    private TextView txtCuenta;
    private TextView txtZona;
    private TextView txtFechaSalida;
    private TextView txtFechaRegreso;
    private TextView txtNoNoches;
    private TextView txtTipoTransporte;
    private TextView txtAnticipo;
    private TextView txtGastosAvion;
    private TextView txtGastosComprobados;
    private TextView txtObservacionesrt;
    private TextView txtStatusrt;

    public static String foliort = "";
    private String svid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_requests_travel);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        FloatingActionButton fabOptions = (FloatingActionButton) findViewById(R.id.options);
        fabOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptionsRequestsTravelDialog();
            }
        });

        /* Getting order number from List */
        foliort = getIntent().getExtras().getString("foliort");

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Fetching requests travrl details from sqlite
        HashMap<String, String> order = db.getRequestsTravelDetails(foliort);
        svid = order.get("svid");
        String foliort = order.get("foliort");
        String fecha_solicitud = order.get("fecha_solicitud");
        String cuenta = order.get("cuenta");
        String zona = order.get("zona");
        String fecha_salida = order.get("fecha_salida");
        String fecha_regreso = order.get("fecha_regreso");
        String no_noches = order.get("no_noches");
        String no_personas = order.get("no_personas");
        String tipo_transporte = order.get("tipo_transporte");
        String anticipo = order.get("anticipo");
        String gastos_avion = order.get("gastos_avion");
        String gastos_comprobados = order.get("gastos_comprobados");
        String observacionesrt = order.get("observacionesrt");
        String statustr = order.get("statustr");

        // Setting values from sqlite to view xml
        txtFoliort = (TextView) findViewById(R.id.foliort_view);
        txtFoliort.setText(foliort);
        txtFechaSolicitud = (TextView) findViewById(R.id.fecha_solicitud_view);
        txtFechaSolicitud.setText(fecha_solicitud);
        txtCuenta = (TextView) findViewById(R.id.cuenta_view);
        txtCuenta.setText(cuenta);
        txtZona = (TextView) findViewById(R.id.zona_view);
        txtZona.setText(zona);
        txtFechaSalida = (TextView) findViewById(R.id.fechasalida_view);
        txtFechaSalida.setText(fecha_salida);
        txtFechaRegreso = (TextView) findViewById(R.id.fecharegreso_view);
        txtFechaRegreso.setText(fecha_regreso);
        txtNoNoches = (TextView) findViewById(R.id.nonoches_view);
        txtNoNoches.setText(no_noches + " / " + no_personas);
        txtTipoTransporte = (TextView) findViewById(R.id.label_tipotransporte_view);
        txtTipoTransporte.setText(tipo_transporte);
        txtAnticipo = (TextView) findViewById(R.id.label_anticipo_view);
        txtAnticipo.setText(anticipo);
        txtGastosAvion = (TextView) findViewById(R.id.label_gastosavion_view);
        txtGastosAvion.setText(gastos_avion);
        txtGastosComprobados = (TextView) findViewById(R.id.label_gastoscomprobados_view);
        txtGastosComprobados.setText(gastos_comprobados);
        txtObservacionesrt = (TextView) findViewById(R.id.label_observacionesrt_view);
        txtObservacionesrt.setText(observacionesrt);
        txtStatusrt = (TextView) findViewById(R.id.label_statusrtrt_view);
        txtStatusrt.setText(statustr);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), ListViewRequestsTravelActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }

    private void showOptionsRequestsTravelDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ManageRequestsTravelActivity.this);
        builder.setTitle(R.string.options_requests_travel_title)
                .setItems(R.array.options_requests_travel_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            case 0: selectItem(0);
                                break;
                            case 1: selectItem(1);
                                break;
                            case 2: selectItem(2);
                                break;
                        }

                    }
                });
        builder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void selectItem(int position) {
        switch(position) {
            case 0:
                goToListReqTravelDetails();
                break;
            case 1:
                goToListReqTravelAirplaineTickets();
                break;
            case 2:
                goToListReqTravelVouchers();
                break;
            default:
        }
    }

    public void goToListReqTravelDetails () {
        if (svid.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el id de la solicitud de servicio!", Toast.LENGTH_LONG)
                    .show();
        } else {
            // Launch ManageOrderActivity activity
            Intent intent = new Intent(
                    ManageRequestsTravelActivity.this,
                    ListReqTravelDetailsActivity.class);
            intent.putExtra("foliort", foliort);

            startActivity(intent);
            finish();
        }
    }

    public void goToListReqTravelAirplaineTickets () {
        if (svid.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el id de la solicitud de servicio!", Toast.LENGTH_LONG)
                    .show();
        } else {
            // Launch ManageOrderActivity activity
            Intent intent = new Intent(
                    ManageRequestsTravelActivity.this,
                    ListReqTravelAirplaneTicketActivity.class);
            intent.putExtra("foliort", foliort);

            startActivity(intent);
            finish();
        }
    }

    public void goToListReqTravelVouchers () {
        if (svid.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el id de la solicitud de servicio!", Toast.LENGTH_LONG)
                    .show();
        } else {
            // Launch ManageOrderActivity activity
            Intent intent = new Intent(
                    ManageRequestsTravelActivity.this,
                    ListReqTravelVouchersActivity.class);
            intent.putExtra("foliort", foliort);

            startActivity(intent);
            finish();
        }
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        // Launching the login activity
        Intent intent = new Intent(ManageRequestsTravelActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }

}
