package mx.hgsoft.mobile.biossmannsos;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 28/06/16.
 */
public class RegisterReqTravelAirplaneTicketActivity extends AppCompatActivity{

    private static final String TAG = RegisterActivity.class.getSimpleName();

    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private SessionManager session;

    private Spinner spinnerOrigen;
    private Spinner spinnerDestino;
    private EditText inputFechaSalida;
    private EditText inputHoraSalida;
    private EditText inputReservacion;
    private EditText inputCosto;

    private String svid = "";
    private String foliort = "";

    private Date date_fechasalida = new Date();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_reqtravel_tickets);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        /* Getting order number from List */
        foliort = getIntent().getExtras().getString("foliort");

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Fetching requests travrl details from sqlite
        HashMap<String, String> order = db.getRequestsTravelDetails(foliort);
        svid = order.get("svid");

        // Setting values from sqlite to view xml
        TextView txtFoliort = (TextView) findViewById(R.id.foliort_view);
        txtFoliort.setText(foliort);

        /* Getting xml elements */
        inputFechaSalida = (EditText) findViewById(R.id.fechasalida);
        inputFechaSalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerOut();
            }
        });

        inputHoraSalida = (EditText) findViewById(R.id.horasalida);
        inputHoraSalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog();
            }
        });


        inputReservacion = (EditText) findViewById(R.id.reservacion);
        inputCosto = (EditText) findViewById(R.id.costo);

        if (foliort.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el folio de la solicitud de viáticos!", Toast.LENGTH_LONG)
                    .show();
        } else {
            getCatAirports();
        }

        Button btnSaveTickets = (Button) findViewById(R.id.btnRegisterReqTravelTickets);

        // Register Button Click event
        btnSaveTickets.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                /* spinner airport origin */
                String spinnerAirportsOString = null;
                spinnerAirportsOString = spinnerOrigen.getSelectedItem().toString();
                int nPosAirportsOSpinner = spinnerOrigen.getSelectedItemPosition();
                String nPosSpinnerAirportsOString = Integer.toString(nPosAirportsOSpinner);
                /* End spinner airport origin */

                /* spinner airport destiny */
                String spinnerAirportsDString = null;
                spinnerAirportsDString = spinnerDestino.getSelectedItem().toString();
                int nPosAirportsDSpinner = spinnerDestino.getSelectedItemPosition();
                String nPosSpinnerAirportsDString = Integer.toString(nPosAirportsDSpinner);
                /* End spinner airport origin */

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
                String fecha_salida = inputFechaSalida.getText().toString().trim();
                try {
                    date_fechasalida = sdf.parse(fecha_salida);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String hora_salida = inputHoraSalida.getText().toString().trim();
                String reservacion = inputReservacion.getText().toString().trim();
                String costo = "";
                if (inputCosto.getText().toString().trim().isEmpty()) {
                    costo = "0.00";
                } else {
                    costo = inputCosto.getText().toString().trim();
                }


                // Fetching user details from sqlite
                HashMap<String, String> user = db.getUserDetails();
                String usrMod = user.get("email");

                WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
                String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                if (checkValidation()) {
                    registerRequestsTravelTickets(svid, nPosSpinnerAirportsOString, nPosSpinnerAirportsDString, fecha_salida, hora_salida, reservacion, costo, usrMod, ip);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Llenar correctamente los campos!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), ListReqTravelAirplaneTicketActivity.class);
        myIntent.putExtra("foliort", foliort);
        db.deleteRequestsTravelTickets();
        db.deleteAirports();
        startActivityForResult(myIntent, 0);
        return true;
    }

    private void showDatePickerOut() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondateOut);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondateOut = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            inputFechaSalida.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(dayOfMonth));
        }
    };

    private void showDatePickerIn() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondateIn);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondateIn = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            inputFechaSalida.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(dayOfMonth));

        }
    };

    public void showTimePickerDialog() {
        TimePickerFragment time = new TimePickerFragment();
        /**
         * Set Up Current Time Into dialog
         */
        Calendar calendar = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("hour", calendar.get(Calendar.HOUR_OF_DAY));
        args.putInt("minute", calendar.get(Calendar.MINUTE));
        time.setArguments(args);
        /**
         * Set Call back to capture selected time
         */
        time.setCallBack(ontimeIn);
        time.show(getFragmentManager(), "Time Picker");

    }

    TimePickerDialog.OnTimeSetListener ontimeIn = new TimePickerDialog.OnTimeSetListener() {

        public void onTimeSet(TimePicker view, int hour, int minute) {

            inputHoraSalida.setText(String.valueOf(hour) + ":" + String.valueOf(minute) + ":00");

        }
    };

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * function to get JSON from API_REST about Catalog's airports
     */
    private void getCatAirports() {
        String tag_string_req = "req_catAirports";

        pDialog.setMessage("");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_AIRPORTS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Airports response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("airports");

                        db.addCatAirports("0", "Elige aeropuerto");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String aeropuertoid = jsonObject.optString("ID_Aeropuerto").toString();
                            String aeropuerto = jsonObject.optString("Aeropuerto").toString();

                            // Inserting row in orders table
                            db.addCatAirports(aeropuertoid, aeropuerto);
                        }
                        populatingSpinnerAirportsO();
                        populatingSpinnerAirportsD();

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void populatingSpinnerAirportsO() {

            /* To populate Spinner, first create an adapter */
        spinnerOrigen = (Spinner) findViewById(R.id.origen_spinner);

        // The desired columns to be bound
        String[] columns = new String[]{
                "airport"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[]{
                android.R.id.text1
        };

        Cursor cursorAirportsO = db.getAirportsListCursor();

        SimpleCursorAdapter adapterAirportsO = new SimpleCursorAdapter(
                this, // context
                android.R.layout.simple_spinner_item, // layout file
                cursorAirportsO, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterAirportsO.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerOrigen.setAdapter(adapterAirportsO);
    }

    private void populatingSpinnerAirportsD() {

            /* To populate Spinner, first create an adapter */
        spinnerDestino = (Spinner) findViewById(R.id.destino_spinner);

        // The desired columns to be bound
        String[] columns = new String[]{
                "airport"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[]{
                android.R.id.text1
        };

        Cursor cursorAirportsD = db.getAirportsListCursor();

        SimpleCursorAdapter adapterAirportsD = new SimpleCursorAdapter(
                this, // context
                android.R.layout.simple_spinner_item, // layout file
                cursorAirportsD, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterAirportsD.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerDestino.setAdapter(adapterAirportsD);
    }

    /**
     * Function to store requeststraveltickets in MySQL database will post
     * */
    private void registerRequestsTravelTickets(final String svid, final String nPosSpinnerAirportsOString, final String nPosSpinnerAirportsDString, final String fecha_salida, final String hora_salida, final String reservacion, final String costo, final String usrMod, final String ip) {
        // Tag used to cancel the request
        String tag_string_req = "req_registerRequestsTravelTickets";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTERREQUESTSTRAVELTICKETS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Tickets Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // Repair successfully stored in MySQL
                        Toast.makeText(getApplicationContext(), "Inserción Exitosa", Toast.LENGTH_LONG).show();
                        db.deleteRequestsTravelTickets();
                        db.deleteTravels();
                        // Launch ManageOrderActivity activity
                        Intent intent = new Intent(
                                RegisterReqTravelAirplaneTicketActivity.this,
                                ListReqTravelAirplaneTicketActivity.class);
                        intent.putExtra("foliort", foliort);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                //Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("svid", svid);
                params.put("ID_AO", nPosSpinnerAirportsOString);
                params.put("ID_AD", nPosSpinnerAirportsDString);
                params.put("Fecha_Salida", fecha_salida);
                params.put("Hora_Salida", hora_salida);
                params.put("Clave_Reservacion", reservacion);
                params.put("Costo", costo);
                params.put("ip", ip);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.selectedElementSpinner(spinnerOrigen)) {
            Toast.makeText(this, "Elije origen", Toast.LENGTH_SHORT).show();
            ret = false;
        }
        if (!Validation.selectedElementSpinner(spinnerDestino)) {
            Toast.makeText(this, "Elije destino", Toast.LENGTH_SHORT).show();
            ret = false;
        }
        if (!Validation.selectedElementsSpinners(spinnerOrigen, spinnerDestino)){
            Toast.makeText(this, "Debe ser diferente el origen del destino", Toast.LENGTH_SHORT).show();
            ret = false;
        }
        if (!Validation.hasText(inputFechaSalida)) ret = false;
        //if (!Validation.hasText(inputHoraSalida)) ret = false;
        //if (!Validation.hasText(inputCosto)) ret = false;
        return ret;
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        db.deleteRequestsTravelDetails();
        db.deleteTravels();
        db.deleteRequestsTravelTickets();
        db.deleteAirports();
        // Launching the login activity
        Intent intent = new Intent(RegisterReqTravelAirplaneTicketActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }
}
