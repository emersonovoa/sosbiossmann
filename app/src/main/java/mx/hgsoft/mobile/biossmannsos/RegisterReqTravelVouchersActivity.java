package mx.hgsoft.mobile.biossmannsos;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 28/06/16.
 */
public class RegisterReqTravelVouchersActivity extends AppCompatActivity{

    private static final String TAG = RegisterReqTravelVouchersActivity.class.getSimpleName();

    private ProgressDialog pDialog;
    ProgressDialog dialog;

    private SQLiteHandler db;
    private SessionManager session;

    private Spinner spinnerViaticos;
    private Spinner spinnerComprobantes;
    private EditText inputFolio;
    private EditText inputFecha;
    private Spinner spinnerProviders;
    //private EditText inputMoneda;
    private EditText inputTC;
    private EditText inputSubtotal;
    private EditText inputImpuestosRetenidos;
    private EditText inputImpuestosTrasladados;

    /* Animation FAB's */
    private Boolean isFabOpen = false;
    private FloatingActionButton addAttachments,addXML,addPDF;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;

    private String svid = "";
    private String foliort = "";

    private Date date_fecha = new Date();

    private static final int PICK_FILE_REQUEST = 1;
    private String SERVER_URL = null;

    //private String selectedFilePath;
    private String selectedKindFile;

    TextView tvFileXML;
    TextView tvFilePDF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        setContentView(R.layout.activity_register_reqtravel_vouchers);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        /* Getting order number from List */
        foliort = getIntent().getExtras().getString("foliort");

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Fetching requests travrl details from sqlite
        HashMap<String, String> order = db.getRequestsTravelDetails(foliort);
        svid = order.get("svid");

        // Setting values from sqlite to view xml
        TextView txtFoliort = (TextView) findViewById(R.id.foliort_view);
        txtFoliort.setText(foliort);

        SERVER_URL = AppConfig.URL_UPLOADS;

        /* Getting xml elements */
        inputFolio = (EditText) findViewById(R.id.comprobante_folio);
        inputFecha = (EditText) findViewById(R.id.comprobante_fecha);
        inputFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerOut();
            }
        });

        //inputMoneda = (EditText) findViewById(R.id.moneda);
        inputTC = (EditText) findViewById(R.id.tc);
        inputSubtotal = (EditText) findViewById(R.id.subtotal);
        inputImpuestosRetenidos = (EditText) findViewById(R.id.impretenidos);
        inputImpuestosTrasladados = (EditText) findViewById(R.id.imptransladados);

        /* Animation FAB's */
        addAttachments = (FloatingActionButton)findViewById(R.id.addAttachments);
        addXML = (FloatingActionButton)findViewById(R.id.addXML);
        addPDF = (FloatingActionButton)findViewById(R.id.addPDF);
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);
        addAttachments.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                animateFAB();
            }
        });
        addXML.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showFileChooserXML();
            }
        });
        addPDF.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showFileChooserPDF();
            }
        });

        tvFileXML = (TextView) findViewById(R.id.tv_file_xml);
        tvFilePDF = (TextView) findViewById(R.id.tv_file_pdf);

        /* End Animation FAB's */

        if (foliort.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el folio de la solicitud de viáticos!", Toast.LENGTH_LONG)
                    .show();
        } else {
            getCatKindOfTravel();
            getCatVouchers();
            getProviders();
        }

        Button btnSaveVouchers = (Button) findViewById(R.id.btnRegisterReqTravelVouchers);

        // Register Button Click event
        btnSaveVouchers.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                String spinnerTravelsString = null;
                spinnerTravelsString = spinnerViaticos.getSelectedItem().toString();
                int nPosTravelsSpinner = spinnerViaticos.getSelectedItemPosition();
                String nPosSpinnerTravelsString = Integer.toString(nPosTravelsSpinner);

                String spinnerVouchersString = null;
                spinnerVouchersString = spinnerComprobantes.getSelectedItem().toString();
                int nPosVouchersSpinner = spinnerComprobantes.getSelectedItemPosition();
                String nPosSpinnerVouchersString = Integer.toString(nPosVouchersSpinner);

                String folio = inputFolio.getText().toString().trim();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
                String fecha = inputFecha.getText().toString().trim();
                try {
                    date_fecha = sdf.parse(fecha);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String spinnerProvidersString = null;
                spinnerProvidersString = spinnerProviders.getSelectedItem().toString();
                int nPosProvidersSpinner = spinnerProviders.getSelectedItemPosition();
                String nPosSpinnerProvidersString = Integer.toString(nPosProvidersSpinner);

                //String moneda = inputMoneda.getText().toString().trim();
                String tc = inputTC.getText().toString().trim();
                String subtotal = inputSubtotal.getText().toString().trim();
                String impretenidos = inputImpuestosRetenidos.getText().toString().trim();
                String imptrasladados = inputImpuestosTrasladados.getText().toString().trim();

                String xml = tvFileXML.getText().toString().trim();
                String pdf = tvFilePDF.getText().toString().trim();

                // Fetching user details from sqlite
                HashMap<String, String> user = db.getUserDetails();
                String usrMod = user.get("email");

                WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
                String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                if (checkValidation()) {
                    registerRequestsTravelVouchers(svid,
                            nPosSpinnerTravelsString,
                            nPosSpinnerVouchersString,
                            folio,
                            fecha,
                            nPosSpinnerProvidersString,
                            //moneda,
                            tc,
                            subtotal,
                            impretenidos,
                            imptrasladados,
                            xml,
                            pdf,
                            usrMod,
                            ip);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Llenar correctamente los campos!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), ListReqTravelVouchersActivity.class);
        myIntent.putExtra("foliort", foliort);
        db.deleteTravels();
        db.deleteVouchers();
        db.deleteProviders();
        db.deleteRequestsTravelVouchers();
        startActivityForResult(myIntent, 0);
        return true;
    }

    private void showDatePickerOut() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondateOut);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondateOut = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            inputFecha.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(dayOfMonth));
        }
    };

    private void showDatePickerIn() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondateIn);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondateIn = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            inputFecha.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(dayOfMonth));

        }
    };

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * function to get JSON from API_REST about Catalog's kind of travel
     */
    private void getCatKindOfTravel() {
        String tag_string_req = "req_catKindOfTravel";

        pDialog.setMessage("");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_KINDOFTRAVELS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Kind of travels response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("travels");

                        db.addCatTravels("0", "Elige tipo de viático");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String viaticoid = jsonObject.optString("ID_TipoViatico").toString();
                            String viatico = jsonObject.optString("Tipo_Viatico").toString();

                            // Inserting row in orders table
                            db.addCatTravels(viaticoid, viatico);
                        }
                        populatingSpinnerTravels();

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to get JSON from API_REST about Catalog's vouchers
     */
    private void getCatVouchers() {
        String tag_string_req = "req_catVouchers";

        pDialog.setMessage("");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CATVOUCHERS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Vouchers response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("vouchers");

                        db.addCatVouchers("0", "Elige tipo de comprobante");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String comprobantetipoid = jsonObject.optString("ID_TipoComprobante").toString();
                            String tipo_comprobante = jsonObject.optString("Tipo_Comprobante").toString();

                            // Inserting row in orders table
                            db.addCatVouchers(comprobantetipoid, tipo_comprobante);
                        }
                        populatingSpinnerVouchers();

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to get JSON from API_REST about Catalog's vouchers
     */
    private void getProviders() {
        String tag_string_req = "req_Providers";

        pDialog.setMessage("");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_PROVIDERS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Providers response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("providers");

                        db.addProviders("0", "Elige proveedor", "Elige proveedor");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String contribuyenteid = jsonObject.optString("ID_Contribuyente").toString();
                            String rfc = jsonObject.optString("RFC").toString();
                            String nombre_contribuyente = jsonObject.optString("Nombre_Contribuyente").toString();

                            // Inserting row in orders table
                            db.addProviders(contribuyenteid, rfc, nombre_contribuyente);
                        }
                        populatingSpinnerProviders();

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void populatingSpinnerTravels() {

            /* To populate Spinner, first create an adapter */
        spinnerViaticos = (Spinner) findViewById(R.id.viatico_spinner);

        // The desired columns to be bound
        String[] columns = new String[]{
                "travel"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[]{
                android.R.id.text1
        };

        Cursor cursorTravels = db.getTravelsListCursor();

        SimpleCursorAdapter adapterTravels = new SimpleCursorAdapter(
                this, // context
                android.R.layout.simple_spinner_item, // layout file
                cursorTravels, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterTravels.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerViaticos.setAdapter(adapterTravels);
    }

    private void populatingSpinnerVouchers() {

            /* To populate Spinner, first create an adapter */
        spinnerComprobantes = (Spinner) findViewById(R.id.comprobante_spinner);

        // The desired columns to be bound
        String[] columns = new String[]{
                "voucher"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[]{
                android.R.id.text1
        };

        Cursor cursorVouchers = db.getVouchersListCursor();

        SimpleCursorAdapter adapterVouchers = new SimpleCursorAdapter(
                this, // context
                android.R.layout.simple_spinner_item, // layout file
                cursorVouchers, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterVouchers.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerComprobantes.setAdapter(adapterVouchers);
    }

    private void populatingSpinnerProviders() {

            /* To populate Spinner, first create an adapter */
        spinnerProviders = (Spinner) findViewById(R.id.proveedor_spinner);

        // The desired columns to be bound
        String[] columns = new String[]{
                "rfc_provider"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[]{
                android.R.id.text1
        };

        Cursor cursorProviders = db.getProvidersListCursor();

        SimpleCursorAdapter adapterProviders = new SimpleCursorAdapter(
                this, // context
                android.R.layout.simple_spinner_item, // layout file
                cursorProviders, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterProviders.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerProviders.setAdapter(adapterProviders);
    }

    /**
     * Function to store requeststraveltickets in MySQL database will post
     * */
    private void registerRequestsTravelVouchers(final String svid,
                                                final String nPosSpinnerTravelsString,
                                                final String nPosSpinnerVouchersString,
                                                final String folio,
                                                final String fecha,
                                                final String spinnerProvidersString,
                                                //final String moneda,
                                                final String tc,
                                                final String subtotal,
                                                final String impretenidos,
                                                final String imptrasladados,
                                                final String xml,
                                                final String pdf,
                                                final String usrMod,
                                                final String ip) {

        // Tag used to cancel the request
        String tag_string_req = "req_registerRequestsTravelVouchers";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTERREQUESTSTRAVELVOUCHERS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Vouchers Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // Repair successfully stored in MySQL
                        Toast.makeText(getApplicationContext(), "Inserción Exitosa", Toast.LENGTH_LONG).show();
                        db.deleteRequestsTravelVouchers();
                        db.deleteTravels();
                        db.deleteVouchers();
                        db.deleteProviders();
                        // Launch ManageOrderActivity activity
                        Intent intent = new Intent(
                                RegisterReqTravelVouchersActivity.this,
                                ListReqTravelVouchersActivity.class);
                        intent.putExtra("foliort", foliort);
                        startActivity(intent);
                        if (!xml.equals("")){
                            uploadFile(xml);
                        }

                        if (!pdf.equals("")){
                            uploadFile(pdf);
                        }
                        finish();
                    } else {
                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                //Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("svid", svid);
                params.put("id_tipo_viatico", nPosSpinnerTravelsString);
                params.put("id_tipo_comprobante", nPosSpinnerVouchersString);
                params.put("folio", folio);
                params.put("fecha", fecha);
                params.put("rfc_proveedor", spinnerProvidersString);
                //params.put("moneda", moneda);
                params.put("tc", tc);
                params.put("subtotal", subtotal);
                params.put("impretenidos", impretenidos);
                params.put("imptrasladados", imptrasladados);
                String[] parts_xml = xml.split("/");
                final String xml_name = parts_xml[parts_xml.length-1];
                params.put("xml", xml_name);
                String[] parts_pdf = pdf.split("/");
                final String pdf_name = parts_pdf[parts_pdf.length-1];
                params.put("pdf", pdf_name);
                params.put("usrMod", usrMod);
                params.put("ip", ip);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void animateFAB(){
        if(isFabOpen){
            addAttachments.startAnimation(rotate_backward);
            addXML.startAnimation(fab_close);
            addPDF.startAnimation(fab_close);
            addXML.setClickable(false);
            addPDF.setClickable(false);
            isFabOpen = false;
        } else {
            addAttachments.startAnimation(rotate_forward);
            addXML.startAnimation(fab_open);
            addPDF.startAnimation(fab_open);
            addXML.setClickable(true);
            addPDF.setClickable(true);
            isFabOpen = true;
        }
    }

    private void showFileChooserXML() {
        selectedKindFile = "XML";
        Intent intent = new Intent();
        //sets the select file to xml types of files
        intent.setType("text/xml");
        /*para pruebas
        intent.setType("image/jpeg");*/
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Elije archivo xml..."), PICK_FILE_REQUEST);
    }

    private void showFileChooserPDF() {
        selectedKindFile = "PDF";
        Intent intent = new Intent();
        //sets the select file to pdf types of files
        intent.setType("application/pdf");
        /*para pruebas
        intent.setType("image/jpeg");*/
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Elije archivo pdf..."), PICK_FILE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == PICK_FILE_REQUEST){
                if(data == null){
                    //no data present
                    return;
                }

                Uri selectedFileUri = data.getData();
                final String selectedFilePath = FileUtils.getPath(this,selectedFileUri);
                Log.i(TAG,"Selected File Path:" + selectedFilePath);

                String[] parts = selectedFilePath.split("/");
                if(selectedFilePath != null && !selectedFilePath.equals("")){
                    if(selectedKindFile.equals("XML")) {
                        final String fileNameXML = parts[parts.length-1];
                        tvFileXML.setText(selectedFilePath);
                        Toast.makeText(this, "Archivo seleccionado XML: " + fileNameXML, Toast.LENGTH_LONG).show();
                        selectedKindFile = "";
                    } else {
                        final String fileNamePDF = parts[parts.length-1];
                        tvFilePDF.setText(selectedFilePath);
                        Toast.makeText(this, "Archivo seleccionado PDF: " + fileNamePDF, Toast.LENGTH_LONG).show();
                        selectedKindFile = "";
                    }
                }else{
                    Toast.makeText(this, "No se ha podido subir el archivo al servidor.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public int uploadFile(final String selectedFilePath){

        final String fileName = selectedFilePath;

        int serverResponseCode = 0;

        HttpURLConnection connection;
        DataOutputStream dataOutputStream;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        int bytesRead,bytesAvailable,bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File selectedFile = new File(selectedFilePath);


        //String[] parts = selectedFilePath.split("/");
        //final String fileName = parts[parts.length-1];

        if (!selectedFile.isFile()){
            //dialog.dismiss();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //tvFileXML.setText("No existe el archivo: " + selectedFilePath);
                    Toast.makeText(RegisterReqTravelVouchersActivity.this, "No existe el archivo: " + selectedFilePath,Toast.LENGTH_SHORT).show();
                }
            });

            return 0;

        }else{
            try{

                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                URL url = new URL(SERVER_URL);
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                connection.setRequestProperty("uploaded_file",fileName);

                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());

                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name='uploaded_file'; filename='" + fileName + "'" + lineEnd);

                dataOutputStream.writeBytes(lineEnd);

                //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable,maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer,0,bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0){
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer,0,bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable,maxBufferSize);
                    bytesRead = fileInputStream.read(buffer,0,bufferSize);
                }

                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();

                Log.i(TAG, "Server Response is: " + serverResponseMessage + ": " + serverResponseCode);

                //response code of 200 indicates the server status OK
                if(serverResponseCode == 200){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(RegisterReqTravelVouchersActivity.this,
                                    "El archivo " + fileName + " se ha agregado completamente.\n\n " +
                                            "Puedes verlo en: \n\n" + "http://192.168.88.15/sos/app_sos_api/uploads/"+ fileName,
                                    Toast.LENGTH_SHORT).show();
                            //tvFileXML.setText("File Upload completed.\n\n You can see the uploaded file here: \n\n" + "http://192.168.88.15/sos/app_sos_api/uploads"+ fileName);
                        }
                    });
                }

                //closing the input and output streams
                fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();



            } catch (FileNotFoundException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(RegisterReqTravelVouchersActivity.this, "Archivo no encontrado.", Toast.LENGTH_SHORT).show();
                        //>>>>>>>>>> Checar porque no hace upload desde el cell
                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                    Toast.makeText(RegisterReqTravelVouchersActivity.this, "URL error!", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                    Toast.makeText(RegisterReqTravelVouchersActivity.this, "No se puede leer el archivo!", Toast.LENGTH_SHORT).show();
            }
            //dialog.dismiss();
            return serverResponseCode;
        }

    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.selectedElementSpinner(spinnerViaticos)) {
            Toast.makeText(this, "Elije tipo de viático", Toast.LENGTH_SHORT).show();
            ret = false;
        }
        if (!Validation.selectedElementSpinner(spinnerComprobantes)) {
            Toast.makeText(this, "Elije tipo de comprobante", Toast.LENGTH_SHORT).show();
            ret = false;
        }
        if (!Validation.hasText(inputFolio)) ret = false;
        if (!Validation.hasText(inputFecha)) ret = false;
        if (!Validation.hasText(inputTC)) ret = false;
        if (!Validation.hasText(inputSubtotal)) ret = false;
        if (!Validation.hasText(inputImpuestosRetenidos)) ret = false;
        if (!Validation.hasText(inputImpuestosTrasladados)) ret = false;
        return ret;
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        db.deleteRequestsTravelDetails();
        db.deleteTravels();
        db.deleteRequestsTravelTickets();
        db.deleteAirports();
        db.deleteRequestsTravelVouchers();
        db.deleteTravels();
        db.deleteVouchers();
        db.deleteProviders();
        // Launching the login activity
        Intent intent = new Intent(RegisterReqTravelVouchersActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }

}
