package mx.hgsoft.mobile.biossmannsos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 14/03/16.
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();
    public static String id_user_st = "";
    public static String name = "";
    public static String email = "";
    public static String pImage = "";

    private Bitmap bitmap;

    private TextView txtName;
    private TextView txtEmail;
    private ImageView imgProfile;
    private Button btnLogout;

    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private SessionManager session;

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;

    private String PROFILE_IMGS_URL = AppConfig.URL_PROFILE_IMGS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        txtName = (TextView) findViewById(R.id.name);
        txtEmail = (TextView) findViewById(R.id.email);
        imgProfile = (ImageView) findViewById(R.id.imageProfile);
        //btnLogout = (Button) findViewById(R.id.btnLogout);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        //Clean all data first
        db.deleteOrdersAttended();
        db.deleteRoutines();
        db.deleteHaveRoutine();
        db.deleteRequestsTravel();
        db.deleteRepairs();
        db.deleteCounts();
        db.deleteRequestsTravel();
        db.deleteRequestsTravelTickets();

        // Fetching user details from sqlite
        HashMap<String, String> user = db.getUserDetails();
        id_user_st = user.get("uid");
        name = user.get("name");
        email = user.get("email");
        pImage = user.get("foto");


        String st_name = "ST. " + name;

        // Displaying the user details on the screen
        txtName.setText(st_name);
        txtEmail.setText(email);

        String urlImg = PROFILE_IMGS_URL + pImage;

        //Displaying the user profile pic
        bitmap = getBitmapFromURL(urlImg);
        imgProfile.setImageBitmap(bitmap);

    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    /* Click al botón órdenes de servicio atendidas */
    public void goToAttendedList () {
        if (id_user_st.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el id del usuario!", Toast.LENGTH_LONG)
                    .show();
        } else {
            getOrdersAttended(id_user_st);
        }
    }

    /* Click al botón órdenes de servicio no atendidas */
    public void goToNoAttendedList () {
        if (id_user_st.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el id del usuario!", Toast.LENGTH_LONG)
                    .show();
        } else {
            getOrdersNoAttended(id_user_st);
        }
    }

    /* Click al botón ver todas las órdenes */
    public void goToAllList () {
        if (id_user_st.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el id del usuario!", Toast.LENGTH_LONG)
                    .show();
        } else {
            getAllOrders(id_user_st);
        }
    }

    /* Click al botón ver solicitudes de viáticos */
    public void goToRequestsTravel () {
        // Launch List Orders Attended Activity
        Intent intent = new Intent(MainActivity.this,
                ListViewRequestsTravelActivity.class);
        startActivity(intent);
        finish();
    }

    /* Click al botón ayuda */
    public void goToHelp() {
        // Launch Help Activity
        Intent intent = new Intent(MainActivity.this,
                HelpActivity.class);
        startActivity(intent);
        finish();
    }

    /* Click al botón acerca de */
    public void goToAbout() {
        // Launch Help Activity
        Intent intent = new Intent(MainActivity.this,
                AboutActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * function to get JSON from API_REST about Orders Attended
     * */
    private void getOrdersAttended(final String id_user_st) {
        // Tag used to cancel the request
        String tag_string_req = "req_ordersAttended";

        pDialog.setMessage("Obteniendo Información...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ORDERSATTENDED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Orders response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("orders");

                        for(int i=0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String ordenid = jsonObject.optString("ID_Orden").toString();
                            String tiposervicioid = jsonObject.optString("ID_TipoServicio").toString();
                            String tiposervicio = jsonObject.optString("TipoServicio").toString();
                            String index_orden = jsonObject.optString("Index").toString();
                            String serie = jsonObject.optString("Serie").toString();
                            String folio = jsonObject.optString("Folio").toString();
                            String no_orden = jsonObject.optString("No_Orden").toString();
                            String fecha_orden = jsonObject.optString("Fecha_Orden").toString();
                            String id_ce = jsonObject.optString("ID_CE").toString();
                            String categoria = jsonObject.optString("ID_CE").toString();
                            String id_contacto = jsonObject.optString("ID_Contacto").toString();
                            String fecha_inicio = jsonObject.optString("Fecha_Inicio").toString();
                            String fecha_fin = jsonObject.optString("Fecha_Fin").toString();
                            String falla_reportada = jsonObject.optString("Falla_reportada").toString();
                            String nombre_usuario = jsonObject.optString("Nombre_Usuario").toString();
                            String nombre_supervisor = jsonObject.optString("Nombre_Supervisor").toString();
                            String anexo = jsonObject.optString("Anexo").toString();
                            String funcionando = jsonObject.optString("Funcionando").toString();
                            String observaciones = jsonObject.optString("Observaciones").toString();
                            String status = jsonObject.optString("ID_Status").toString();
                            String fecha_cierre = jsonObject.optString("Fecha_Cierre").toString();
                            String fecha_alta = jsonObject.optString("Fecha_Alta").toString();
                            String usr_alta = jsonObject.optString("Usr_Alta").toString();
                            String usr_mod = jsonObject.optString("Usr_Mod").toString();
                            String sku = jsonObject.optString("SKU").toString();
                            String sku_fabricante = jsonObject.optString("SKU_Fabricante").toString();
                            String modelo = jsonObject.optString("Modelo");
                            String integrado = jsonObject.optString("Integrado").toString();
                            String sku_padre = jsonObject.optString("SKU_Padre").toString();
                            String sku_fabricante_padre = jsonObject.optString("SKU_Padre_Fabricante").toString();
                            String modelo_padre = jsonObject.optString("Modelo_Padre").toString();


                            // Inserting row in orders table
                            db.addOrdesByUser(ordenid, tiposervicioid, tiposervicio, index_orden, serie, folio,
                                    no_orden, fecha_orden, id_ce, categoria, id_contacto, fecha_inicio,
                                    fecha_fin, falla_reportada, nombre_usuario, nombre_supervisor, anexo,
                                    funcionando, observaciones, status, fecha_cierre, fecha_alta,
                                    usr_alta, usr_mod, sku, sku_fabricante, modelo, integrado,
                                    sku_padre, sku_fabricante_padre, modelo_padre);
                        }

                        // Launch List Orders Attended Activity
                        Intent intent = new Intent(MainActivity.this,
                                ListViewOrdersAttendedActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error in orders. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_st", id_user_st);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to get JSON from API_REST about Orders Not Attended
     * */
    private void getOrdersNoAttended(final String id_user_st) {
        // Tag used to cancel the request
        String tag_string_req = "req_ordersNoAttended";

        pDialog.setMessage("Obteniendo Información...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ORDERSNOATTENDED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Orders response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("orders");

                        for(int i=0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String ordenid = jsonObject.optString("ID_Orden").toString();
                            String tiposervicioid = jsonObject.optString("ID_TipoServicio").toString();
                            String tiposervicio = jsonObject.optString("TipoServicio").toString();
                            String index_orden = jsonObject.optString("Index").toString();
                            String serie = jsonObject.optString("Serie").toString();
                            String folio = jsonObject.optString("Folio").toString();
                            String no_orden = jsonObject.optString("No_Orden").toString();
                            String fecha_orden = jsonObject.optString("Fecha_Orden").toString();
                            String id_ce = jsonObject.optString("ID_CE").toString();
                            String categoria = jsonObject.optString("ID_CE").toString();
                            String id_contacto = jsonObject.optString("ID_Contacto").toString();
                            String fecha_inicio = jsonObject.optString("Fecha_Inicio").toString();
                            String fecha_fin = jsonObject.optString("Fecha_Fin").toString();
                            String falla_reportada = jsonObject.optString("Falla_reportada").toString();
                            String nombre_usuario = jsonObject.optString("Nombre_Usuario").toString();
                            String nombre_supervisor = jsonObject.optString("Nombre_Supervisor").toString();
                            String anexo = jsonObject.optString("Anexo").toString();
                            String funcionando = jsonObject.optString("Funcionando").toString();
                            String observaciones = jsonObject.optString("Observaciones").toString();
                            String status = jsonObject.optString("ID_Status").toString();
                            String fecha_cierre = jsonObject.optString("Fecha_Cierre").toString();
                            String fecha_alta = jsonObject.optString("Fecha_Alta").toString();
                            String usr_alta = jsonObject.optString("Usr_Alta").toString();
                            String usr_mod = jsonObject.optString("Usr_Mod").toString();
                            String sku = jsonObject.optString("SKU").toString();
                            String sku_fabricante = jsonObject.optString("SKU_Fabricante").toString();
                            String modelo = jsonObject.optString("Modelo").toString();
                            String integrado = jsonObject.optString("Integrado").toString();
                            String sku_padre = jsonObject.optString("SKU_Padre").toString();
                            String sku_fabricante_padre = jsonObject.optString("SKU_Padre_Fabricante").toString();
                            String modelo_padre = jsonObject.optString("Modelo_Padre").toString();


                            // Inserting row in orders table
                            db.addOrdesByUser(ordenid, tiposervicioid, tiposervicio, index_orden, serie, folio,
                                    no_orden, fecha_orden, id_ce, categoria, id_contacto, fecha_inicio,
                                    fecha_fin, falla_reportada, nombre_usuario, nombre_supervisor, anexo,
                                    funcionando, observaciones, status, fecha_cierre, fecha_alta,
                                    usr_alta, usr_mod, sku, sku_fabricante, modelo, integrado,
                                    sku_padre, sku_fabricante_padre, modelo_padre);
                        }

                        // Launch List Orders No Attended Activity
                        Intent intent = new Intent(MainActivity.this,
                                ListViewOrdersNoAttendedActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error in orders. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_st", id_user_st);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to get JSON from API_REST about Orders Attended
     * */
    private void getAllOrders(final String id_user_st) {
        // Tag used to cancel the request
        String tag_string_req = "req_allOrders";

        pDialog.setMessage("Obteniendo Información...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ALLORDERS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Orders response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("orders");

                        for(int i=0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String ordenid = jsonObject.optString("ID_Orden").toString();
                            String tiposervicioid = jsonObject.optString("ID_TipoServicio").toString();
                            String tiposervicio = jsonObject.optString("TipoServicio").toString();
                            String index_orden = jsonObject.optString("Index").toString();
                            String serie = jsonObject.optString("Serie").toString();
                            String folio = jsonObject.optString("Folio").toString();
                            String no_orden = jsonObject.optString("No_Orden").toString();
                            String fecha_orden = jsonObject.optString("Fecha_Orden").toString();
                            String id_ce = jsonObject.optString("ID_CE").toString();
                            String categoria = jsonObject.optString("ID_CE").toString();
                            String id_contacto = jsonObject.optString("ID_Contacto").toString();
                            String fecha_inicio = jsonObject.optString("Fecha_Inicio").toString();
                            String fecha_fin = jsonObject.optString("Fecha_Fin").toString();
                            String falla_reportada = jsonObject.optString("Falla_reportada").toString();
                            String nombre_usuario = jsonObject.optString("Nombre_Usuario").toString();
                            String nombre_supervisor = jsonObject.optString("Nombre_Supervisor").toString();
                            String anexo = jsonObject.optString("Anexo").toString();
                            String funcionando = jsonObject.optString("Funcionando").toString();
                            String observaciones = jsonObject.optString("Observaciones").toString();
                            String status = jsonObject.optString("ID_Status").toString();
                            String fecha_cierre = jsonObject.optString("Fecha_Cierre").toString();
                            String fecha_alta = jsonObject.optString("Fecha_Alta").toString();
                            String usr_alta = jsonObject.optString("Usr_Alta").toString();
                            String usr_mod = jsonObject.optString("Usr_Mod").toString();
                            String sku = jsonObject.optString("SKU").toString();
                            String sku_fabricante = jsonObject.optString("SKU_Fabricante").toString();
                            String modelo = jsonObject.optString("Modelo").toString();
                            String integrado = jsonObject.optString("Integrado").toString();
                            String sku_padre = jsonObject.optString("SKU_Padre").toString();
                            String sku_fabricante_padre = jsonObject.optString("SKU_Padre_Fabricante").toString();
                            String modelo_padre = jsonObject.optString("Modelo_Padre").toString();


                            // Inserting row in orders table
                            db.addOrdesByUser(ordenid, tiposervicioid, tiposervicio, index_orden, serie, folio,
                                    no_orden, fecha_orden, id_ce, categoria, id_contacto, fecha_inicio,
                                    fecha_fin, falla_reportada, nombre_usuario, nombre_supervisor, anexo,
                                    funcionando, observaciones, status, fecha_cierre, fecha_alta,
                                    usr_alta, usr_mod, sku, sku_fabricante, modelo, integrado,
                                    sku_padre, sku_fabricante_padre, modelo_padre);
                        }

                        // Launch List Orders Attended Activity
                        Intent intent = new Intent(MainActivity.this,
                                ListViewAllOrdersActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error in orders. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_st", id_user_st);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteOrdersAttended();
        db.deleteRoutines();
        db.deleteHaveRoutine();
        db.deleteRepairs();
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        db.deleteRequestsTravelDetails();
        db.deleteTravels();
        db.deleteRequestsTravelTickets();
        db.deleteAirports();
        db.deleteRequestsTravelVouchers();
        db.deleteTravels();
        db.deleteVouchers();
        db.deleteProviders();

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void addDrawerItems() {
        String[] osArray = {
                "ORDENES POR ATENDER",
                "ORDENES ATENDIDAS",
                "TODAS LAS ORDENES",
                "SOLICITUDES DE VIÁTICOS",
                "CERRAR SESIÓN",
                "AYUDA",
                "CENTRO DE AYUDA",
                "ACERCA DE"
            };

        Integer[] imgid ={
                R.drawable.ic_event_busy_black_48dp,
                R.drawable.ic_event_available_black_48dp,
                R.drawable.ic_event_note_black_48dp,
                R.drawable.ic_business_center_black_48dp,
                R.drawable.ic_settings_power_black_48dp,
                R.drawable.ic_help_black_48dp,
                R.drawable.ic_help_black_48dp,
                R.drawable.ic_sos_black_48dp
            };

        CustomListAdapter mAdapter = new CustomListAdapter(this, osArray, imgid);
        mDrawerList=(ListView)findViewById(R.id.navList);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });
    }

    private void selectItem(int position) {
        switch(position) {
            case 0:
                goToNoAttendedList();
                break;
            case 1:
                goToAttendedList();
                break;
            case 2:
                goToAllList();
                break;
            case 3:
                goToRequestsTravel();
                break;
            case 4:
                logoutUser();
                break;
            case 6:
                goToHelp();
                break;
            case 7:
                goToAbout();
                break;
            default:
        }
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Menú");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }

}
