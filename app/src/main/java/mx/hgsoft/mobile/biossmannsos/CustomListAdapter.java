package mx.hgsoft.mobile.biossmannsos;

/**
 * Created by resident on 05/07/16.
 */
import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] itemname;
    private final Integer[] imgid;

    public CustomListAdapter(Activity context, String[] itemname, Integer[] imgid) {
        super(context, R.layout.menu_list, itemname);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.itemname=itemname;
        this.imgid=imgid;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.menu_list, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.item);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        //TextView extratxt = (TextView) rowView.findViewById(R.id.textView1);

        if(position == 5) {
            txtTitle.setText(itemname[position]);
            //rowView.setBackgroundColor(Color.parseColor("#ACCC5F"));
            rowView.setBackgroundColor(Color.parseColor("#e7e7e7"));
            //imageView.setImageResource(imgid[position]);
            //extratxt.setText("Description "+itemname[position]);
        } else {
            txtTitle.setText(itemname[position]);
            imageView.setImageResource(imgid[position]);
            //extratxt.setText("Description "+itemname[position]);
        }


        return rowView;

    };
}
