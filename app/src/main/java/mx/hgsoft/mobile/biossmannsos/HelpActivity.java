package mx.hgsoft.mobile.biossmannsos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 14/03/16.
 */

public class HelpActivity extends AppCompatActivity {

    private static final String TAG = HelpActivity.class.getSimpleName();
    private SQLiteHandler db;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        addOptionsItems();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }

    private void addOptionsItems() {
        String[] optionsArray = {
                "PASOS PARA REALIZAR UNA ÓRDEN DE SERVICIO",
                "PASOS PARA AGREGAR REFACCIONES A UNA ÓRDEN DE SERVICIO",
                "PASOS PARA AGREGAR SOLICITUD DE VIÁTICOS",
                "PASOS PARA AGREGAR DETALLES A UNA SOLICITUD",
                "PASOS PARA AGREGAR BOLETOS DE AVIÓN A UNA SOLICITUD",
                "PASOS PARA AGREGAR COMPROBANTES A UNA SOLICITUD",
                "¿DE DÓNDE OBTENGO MIS ARCHIVOS XML/PDF?",
                "¿CÓMO SALIR DE LA APP?"
        };

        Integer[] imgid ={
                R.drawable.ic_chevron_right_black_24dp,
                R.drawable.ic_chevron_right_black_24dp,
                R.drawable.ic_chevron_right_black_24dp,
                R.drawable.ic_chevron_right_black_24dp,
                R.drawable.ic_chevron_right_black_24dp,
                R.drawable.ic_chevron_right_black_24dp,
                R.drawable.ic_chevron_right_black_24dp,
                R.drawable.ic_chevron_right_black_24dp
        };

        CustomListAdapterHelp clAdapter = new CustomListAdapterHelp(this, optionsArray, imgid);
        ListView optionsList = (ListView)findViewById(R.id.navOptionsList);
        optionsList.setAdapter(clAdapter);

        optionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });
    }

    private void selectItem(int position) {
        switch(position) {
            case 0:
                goToInstructive(0);
                break;
            case 1:
                goToInstructive(1);
                break;
            case 2:
                goToInstructive(2);
                break;
            case 3:
                goToInstructive(3);
                break;
            case 4:
                goToInstructive(4);
                break;
            case 5:
                goToInstructive(5);
                break;
            case 6:
                goToInstructive(6);
                break;
            case 7:
                goToInstructive(7);
                break;
            default:
        }
    }

    /* Click al botón ayuda */
    public void goToInstructive(final int caseoption) {
        // Launch Help Activity
        Intent intent = new Intent(HelpActivity.this,
                InstructiveActivity.class);
        intent.putExtra("case", caseoption);
        startActivity(intent);
        finish();
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteOrdersAttended();
        db.deleteRoutines();
        db.deleteHaveRoutine();
        db.deleteRepairs();
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        db.deleteRequestsTravelDetails();
        db.deleteTravels();
        db.deleteRequestsTravelTickets();
        db.deleteAirports();
        db.deleteRequestsTravelVouchers();
        db.deleteTravels();
        db.deleteVouchers();
        db.deleteProviders();

        // Launching the login activity
        Intent intent = new Intent(HelpActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }

}
