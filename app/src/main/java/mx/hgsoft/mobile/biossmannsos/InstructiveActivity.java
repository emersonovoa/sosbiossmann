package mx.hgsoft.mobile.biossmannsos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 14/03/16.
 */

public class InstructiveActivity extends AppCompatActivity {

    private static final String TAG = InstructiveActivity.class.getSimpleName();
    private SQLiteHandler db;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructive);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        TextView option0 = (TextView) findViewById(R.id.textViewPasosOrden);
        TextView option1 = (TextView) findViewById(R.id.textViewPasosRefacciones);
        TextView option2 = (TextView) findViewById(R.id.textViewPasosViaticos);
        TextView option3 = (TextView) findViewById(R.id.textViewPasosDetSol);
        TextView option4 = (TextView) findViewById(R.id.textViewPasosAvionSol);
        TextView option5 = (TextView) findViewById(R.id.textViewPasosComSol);
        TextView option6 = (TextView) findViewById(R.id.textViewXmlPdf);
        TextView option7 = (TextView) findViewById(R.id.textViewSalir);

        int option = getIntent().getExtras().getInt("case");
        System.out.println("Case option: " + option);

        switch (option){
            case 0:
                actionBar.setTitle("Pasos para realizar una órden de servicio");
                option0.setVisibility(View.VISIBLE);
                option0.setMovementMethod(new ScrollingMovementMethod());
                break;
            case 1:
                actionBar.setTitle("Pasos para agregar refacciones");
                option1.setVisibility(View.VISIBLE);
                option1.setMovementMethod(new ScrollingMovementMethod());
                /* Resize first textviews */
                option0.setHeight(0);
                break;
            case 2:
                actionBar.setTitle("Pasos para agregar solicitud de viáticos");
                option2.setVisibility(View.VISIBLE);
                option2.setMovementMethod(new ScrollingMovementMethod());
                /* Resize first textviews */
                option0.setHeight(0);
                option1.setHeight(0);
                break;
            case 3:
                actionBar.setTitle("Pasos para agregar detalles a una solicitud");
                option3.setVisibility(View.VISIBLE);
                option3.setMovementMethod(new ScrollingMovementMethod());
                /* Resize first textviews */
                option0.setHeight(0);
                option1.setHeight(0);
                option2.setHeight(0);
                break;
            case 4:
                actionBar.setTitle("Pasos para agregar boletos de avión a una solicitud");
                option4.setVisibility(View.VISIBLE);
                option4.setMovementMethod(new ScrollingMovementMethod());
                /* Resize first textviews */
                option0.setHeight(0);
                option1.setHeight(0);
                option2.setHeight(0);
                option3.setHeight(0);
                break;
            case 5:
                actionBar.setTitle("Pasos para agregar comprobantes a una solicitud");
                option5.setVisibility(View.VISIBLE);
                option5.setMovementMethod(new ScrollingMovementMethod());
                /* Resize first textviews */
                option0.setHeight(0);
                option1.setHeight(0);
                option2.setHeight(0);
                option3.setHeight(0);
                option4.setHeight(0);
                break;
            case 6:
                actionBar.setTitle("¿De dónde obtengo mis archivos XML y PDF?");
                option6.setVisibility(View.VISIBLE);
                option6.setMovementMethod(new ScrollingMovementMethod());
                /* Resize first textviews */
                option0.setHeight(0);
                option1.setHeight(0);
                option2.setHeight(0);
                option3.setHeight(0);
                option4.setHeight(0);
                option5.setHeight(0);
                break;
            case 7:
                actionBar.setTitle("¿Cómo salir de la APP?");
                option7.setVisibility(View.VISIBLE);
                option7.setMovementMethod(new ScrollingMovementMethod());
                /* Resize first textviews */
                option0.setHeight(0);
                option1.setHeight(0);
                option2.setHeight(0);
                option3.setHeight(0);
                option4.setHeight(0);
                option5.setHeight(0);
                option6.setHeight(0);
                break;
            default:
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), HelpActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }


    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteOrdersAttended();
        db.deleteRoutines();
        db.deleteHaveRoutine();
        db.deleteRepairs();
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        db.deleteRequestsTravelDetails();
        db.deleteTravels();
        db.deleteRequestsTravelTickets();
        db.deleteAirports();
        db.deleteRequestsTravelVouchers();
        db.deleteTravels();
        db.deleteVouchers();
        db.deleteProviders();

        // Launching the login activity
        Intent intent = new Intent(InstructiveActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }

}
