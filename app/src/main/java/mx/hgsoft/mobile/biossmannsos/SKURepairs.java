package mx.hgsoft.mobile.biossmannsos;

/**
 * Created by resident on 25/10/16.
 */
public class SKURepairs {

    public String sku;
    public String description;

    // constructor for adding sample data
    public SKURepairs(String sku, String description){

        this.sku = sku;
        this.description = description;
    }

}
