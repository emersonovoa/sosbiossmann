package mx.hgsoft.mobile.biossmannsos;
/**
 * Created by resident on 14/03/16.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

public class UpdateRepairsActivity extends AppCompatActivity {
    private static final String TAG = UpdateRepairsActivity.class.getSimpleName();

    /*
     * Change to type CustomAutoCompleteView instead of AutoCompleteTextView
     * since we are extending to customize the view and disable filter
     * The same with the XML view, type will be CustomAutoCompleteView
    */

    CustomAutoCompleteViewSKURepairs autoCompleteViewSKURepairs;

    // adapter for auto-complete
    ArrayAdapter<String> adapterSKURepairs;

    // just to add some initial value
    String[] repair = new String[] {"Refacciones..."};

    public static String no_orden;
    public static String id_or;
    public static String sku_repair;

    private TextView txtTipoServicio_r;
    private TextView txtEquipo_r;
    private TextView txtNoOrden_r;

    private Button btnUpdateRepairs;
    private EditText inputCantidad;
    private EditText inputUnitario;
    private EditText inputTotal;
    private Spinner sppinerActivo;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    private String id_orden;
    private String sku_order;
    private String sku;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_register);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        no_orden = intent.getExtras().getString("no_orden");
        id_or = intent.getExtras().getString("id_or");

        inputCantidad = (EditText) findViewById(R.id.cantidad);
        inputUnitario = (EditText) findViewById(R.id.unitario);
        inputTotal = (EditText) findViewById(R.id.total);

        btnUpdateRepairs = (Button) findViewById(R.id.btnUpdateRepairs);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Fetching order details from sqlite
        HashMap<String, String> order_details = db.getOrderDetails(no_orden);
        id_orden = order_details.get("id_orden");
        String tipo_servicio = order_details.get("tipo_servicio");
        String equipo = order_details.get("sku_fabricante");
        sku_order = order_details.get("sku");

        // Setting values from sqlite to view xml
        txtTipoServicio_r = (TextView) findViewById(R.id.tiposervicio_view_r);
        txtTipoServicio_r.setText(tipo_servicio);
        txtEquipo_r = (TextView) findViewById(R.id.equipo_view_r);
        txtEquipo_r.setText(equipo);
        txtNoOrden_r = (TextView) findViewById(R.id.noorden_view_r);
        txtNoOrden_r.setText(no_orden);

        // Fetching reapirs details from sqlite
        HashMap<String, String> repair = db.getRepairDetails(id_or);
        String cantidad_repair = repair.get("cantidad");
        sku_repair = repair.get("sku_r");
        String unitario_repair = repair.get("unitario");
        String total_repair = repair.get("total");
        String activo_repair = repair.get("activo");
        inputCantidad.setText(cantidad_repair);
        inputUnitario.setText(unitario_repair);
        inputTotal.setText(total_repair);

        sppinerActivo = (Spinner) findViewById(R.id.activo_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.activo_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sppinerActivo.setAdapter(adapter);

        if (!activo_repair.equals(null)) {
            String activo_val = "";
            if (activo_repair.equals("No")) {
                activo_val = "Activo: No";
            } else {
                activo_val = "Activo: Si";
            }
            int spinnerPosition = adapter.getPosition(activo_val);
            sppinerActivo.setSelection(spinnerPosition);
        }

        getSKURepairs(sku_order);

        // Register Button Click event
        btnUpdateRepairs.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                /* Getting order number from List */
                String cantidad = inputCantidad.getText().toString().trim();
                /* SKU */
                sku = autoCompleteViewSKURepairs.getText().toString().trim();
                String sku_update = "";
                Pattern pattern = Pattern.compile(":(.*?):");
                Matcher matcher = pattern.matcher(sku);
                if (matcher.find()) {
                    sku_update = matcher.group(1);
                }
                /* End SKU */

                String unitario = inputUnitario.getText().toString().trim();
                String total = inputTotal.getText().toString().trim();
                String spinnerString = null;
                spinnerString = sppinerActivo.getSelectedItem().toString();
                int nPosSpinner = sppinerActivo.getSelectedItemPosition();
                String nPosSpinnerString = Integer.toString(nPosSpinner);
                // Fetching user details from sqlite
                HashMap<String, String> user = db.getUserDetails();
                String usrMod = user.get("email");
                WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
                String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                if ( checkValidation () ) {
                    updateRepair(id_or, cantidad, sku_update, unitario, total, nPosSpinnerString, usrMod, ip);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Llenar los campos!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), ListViewRepairsActivity.class);
        db.deleteRepairs();
        db.deleteSKURepairs();
        myIntent.putExtra("no_orden", no_orden);
        startActivityForResult(myIntent, 0);
        return true;
    }

    /**
     * Function to store repair in MySQL database will post params(cantidad, sku,
     * unitario, total, nPos) to register url
     * */
    private void updateRepair(final String id_or, final String cantidad, final String sku,
                              final String unitario, final String total, final String nPosSpinnerString,
                              final String usrMod, final String ip) {


        // Tag used to cancel the request
        String tag_string_req = "req_registerRepair";

        pDialog.setMessage("Updating ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEREPAIRS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Update Repairs Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // Repair successfully stored in MySQL
                        Toast.makeText(getApplicationContext(), "Actualización Exitosa", Toast.LENGTH_LONG).show();
                        db.deleteRepairs();
                        db.deleteSKURepairs();
                        // Launch ManageOrderActivity activity
                        Intent intent = new Intent(
                                UpdateRepairsActivity.this,
                                ListViewRepairsActivity.class);
                        intent.putExtra("no_orden", no_orden);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Update Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_or", id_or);
                params.put("cantidad", cantidad);
                params.put("sku", sku);
                params.put("unitario", unitario);
                params.put("total", total);
                params.put("activo", nPosSpinnerString);
                params.put("usrMod", usrMod);
                params.put("ip", ip);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText(inputCantidad)) ret = false;
        if (!Validation.hasTextAutocomplete(autoCompleteViewSKURepairs)) ret = false;
        //if (!Validation.hasText(inputUnitario)) ret = false;
        //if (!Validation.hasText(inputTotal)) ret = false;
        return ret;
    }

    /**
     * function to get JSON from API_REST about SKU Repairs by order SKU
     * */
    private void getSKURepairs(final String sku) {
        // Tag used to cancel the request
        String tag_string_req = "req_SKU_Routines";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SKU_REPAIRS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "SKU Repairs response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("sku_repairs");

                        for(int i=0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String sku = jsonObject.optString("SKU").toString();
                            String descripcion = jsonObject.optString("Descripcion").toString();
                            // Inserting row in routines table
                            db.addSKURepairs(sku, descripcion);
                        }
                        populatingSKURepairs();
                    } else {
                        // Error in orders. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("sku", sku);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void populatingSKURepairs() {

        autoCompleteViewSKURepairs = (CustomAutoCompleteViewSKURepairs) findViewById(R.id.autocompleteSKU);

        // add the listener so it will tries to suggest while the user types
        autoCompleteViewSKURepairs.addTextChangedListener(new CustomAutoCompleteTextChangedListenerSKURepairsEdit(this));

        // set our adapter
        adapterSKURepairs = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, repair);
        autoCompleteViewSKURepairs.setAdapter(adapterSKURepairs);

        List<SKURepairs> list_sku_repairs_edit = db.readSKURepairsEdit(sku_repair);
        int rowCountEdit = list_sku_repairs_edit.size();

        String[] item_edit = new String[rowCountEdit];
        int y = 0;

        for (SKURepairs record_edit : list_sku_repairs_edit) {
            item_edit[y] = ":" + record_edit.sku + ": " + record_edit.description;
            if (!item_edit[y].equals(null)) {
                autoCompleteViewSKURepairs.setText(item_edit[y]);
            }
            y++;
        }
    }

    // this function is used in CustomAutoCompleteTextChangedListenerAirports.java
    public String[] getSKURepairsFromDb(String searchTerm){
        // add items on the array dynamically
        List<SKURepairs> list_sku_repairs = db.readSKURepairs(searchTerm);
        int rowCount = list_sku_repairs.size();

        String[] item = new String[rowCount];
        int x = 0;

        for (SKURepairs record : list_sku_repairs) {
            item[x] = ":" + record.sku + ": " + record.description;
            x++;
        }
        return item;
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteOrdersAttended();
        db.deleteRoutines();
        db.deleteRepairs();
        db.deleteSKURepairs();
        // Launching the login activity
        Intent intent = new Intent(UpdateRepairsActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }
}

