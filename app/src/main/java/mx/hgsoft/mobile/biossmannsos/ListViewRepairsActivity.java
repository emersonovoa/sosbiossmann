package mx.hgsoft.mobile.biossmannsos;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 04/05/16.
 */
public class ListViewRepairsActivity extends AppCompatActivity {

    private static final String TAG = ListViewRepairsActivity.class.getSimpleName();

    public static String id_orden = "";
    public static String no_orden = "";

    /* XML elements view */
    private TextView txtTipoServiciolr;
    private TextView txtEquipolr;
    private TextView txtNoOrdenlr;

    private SQLiteHandler db;
    private SimpleCursorAdapter dataAdapter;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_repairs);

        /* Getting order number from List */
        Intent intent = getIntent();
        no_orden = getIntent().getExtras().getString("no_orden");

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.addRepair);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Launch Form Register Repair Activity
                Intent intent = new Intent(ListViewRepairsActivity.this,
                        RegisterRepairsActivity.class);
                intent.putExtra("no_orden", no_orden);
                startActivity(intent);
                finish();
            }
        });

        // Activate SQLite database handler
        db = new SQLiteHandler(getApplicationContext());
        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Fetching order details from sqlite
        HashMap<String, String> order = db.getOrderDetails(no_orden);
        id_orden = order.get("id_orden");
        String tipo_servicio = order.get("tipo_servicio");
        String equipo = order.get("sku_fabricante");
        String status = order.get("status");

        //Validación si es visible o no el botón
        if (!status.equals("Abierta") ) {
            fab.setVisibility(View.INVISIBLE);

        }

        // Setting values from sqlite to view xml
        txtTipoServiciolr = (TextView) findViewById(R.id.tiposervicio_view_lr);
        txtTipoServiciolr.setText(tipo_servicio);
        txtEquipolr = (TextView) findViewById(R.id.equipo_view_lr);
        txtEquipolr.setText(equipo);
        txtNoOrdenlr = (TextView) findViewById(R.id.noorden_view_lr);
        txtNoOrdenlr.setText(no_orden);

        getRepairsByOrder(id_orden);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), ManageOrderActivity.class);
        myIntent.putExtra("no_orden", no_orden);
        startActivityForResult(myIntent, 0);
        db.deleteRepairs();
        return true;
    }

    /**
     * function to get JSON from API_REST about Repairs by id_orden
     *
     * @param id_orden*/
    private void getRepairsByOrder(final String id_orden) {
        // Tag used to cancel the request
        String tag_string_req = "req_Repairs";

        //pDialog.setMessage("Obteniendo Información...");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REPAIRS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Repairs response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("repairs");

                        for(int i=0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id_or = jsonObject.optString("ID_OR").toString();
                            String id_orden_js = jsonObject.optString("ID_Orden").toString();
                            String cantidad = jsonObject.optString("Cantidad").toString();
                            String sku_js = jsonObject.optString("SKU").toString();
                            String unitario = jsonObject.optString("Unitario").toString();
                            String total = jsonObject.optString("Total").toString();
                            String activo = jsonObject.optString("Activo").toString();
                            String fecha_mod = jsonObject.optString("Fecha_Mod").toString();

                            // Inserting row in repairs table sqlite
                            db.addRepairs(id_or, id_orden_js, cantidad, sku_js, unitario, total, activo, fecha_mod);
                        }
                        //Generate ListView from SQLite Database
                        displayListViewRepairs();

                    } else {
                        // Error in orders. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting parameters to repairs url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_orden", id_orden);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void displayListViewRepairs() {


        Cursor cursor = db.getRepairsListCursor();

        // The desired columns to be bound
        String[] columns = new String[] {
                "cantidad",
                "sku_r",
                "unitario",
                "total",
                "activo",
                "fecha_mod"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[] {
                R.id.cantidad,
                R.id.sku,
                R.id.unitario,
                R.id.total,
                R.id.activo,
                R.id.fecha_mod
        };

        // create the adapter using the cursor pointing to the desired data
        //as well as the layout information
        dataAdapter = new SimpleCursorAdapter(
                this, R.layout.rows_repairs,
                cursor,
                columns,
                to,
                0);

        ListView listView = (ListView) findViewById(R.id.listviewrepairs);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                // Get the cursor, positioned to the corresponding row in the result set
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);

                // Get the order number from this row in the database.
                String orderRepair =
                        cursor.getString(cursor.getColumnIndexOrThrow("repairid"));
                Toast.makeText(getApplicationContext(), "Editar", Toast.LENGTH_SHORT).show();
                // Launch Edit Repair
                Intent intent = new Intent(ListViewRepairsActivity.this,
                        UpdateRepairsActivity.class);
                intent.putExtra("no_orden", no_orden);
                intent.putExtra("id_or", orderRepair);
                startActivity(intent);
            }
        });

        dataAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence constraint) {
                try {
                    return db.fetchRequestsTravelByNumber(constraint.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        session.setLogin(false);
        db.deleteUsers();
        db.deleteOrdersAttended();
        db.deleteHaveRoutine();
        db.deleteRoutines();
        db.deleteRepairs();
        // Launching the login activity
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }

}
