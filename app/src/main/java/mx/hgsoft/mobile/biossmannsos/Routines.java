package mx.hgsoft.mobile.biossmannsos;

/**
 * Created by resident on 08/07/16.
 */
public class Routines {

    String name;
    boolean have_check;
    boolean checkbox;

    public Routines() {
         /*Empty Constructor*/
    }

    public  Routines(String routine, boolean have_check, boolean status){
        this.name = routine;
        this.have_check = have_check;
        this.checkbox = status;
    }
    //Getter and Setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheckbox() {
        return checkbox;
    }

    public boolean haveCheckbox() {
        return have_check;
    }

    public void setCheckbox(boolean checkbox) {
        this.checkbox = checkbox;
    }
}
