package mx.hgsoft.mobile.biossmannsos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 14/04/16.
 */
public class ManageOrderActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();

    public static String id_orden = "";
    public static String id_tiposervicio = "";
    public static String tiposervicio = "";
    public static String no_orden = "";
    public static String sku = "";
    public static String sku_fabricante = "";
    public static String modelo = "";

    public static String integrado = "";

    public static String sku_padre = "";
    public static String id_ce = "";

    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private SessionManager session;

    /* XML elements view */
    private TextView txtTipoServicio;
    private TextView txtEquipo;
    private TextView txtNoOrden;
    private TextView txtFechaOrden;
    private TextView txtFechaInicio;
    private TextView txtFechaFin;
    private TextView txtFallaReportada;
    private TextView txtNombreUsuario;
    private TextView txtNombreSupervisor;
    private TextView txtFuncionando;
    private TextView txtEstatus;
    private Button do_routine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_orders_attended);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        /* Getting order number from List */
        Intent intent = getIntent();
        no_orden = getIntent().getExtras().getString("no_orden");

        actionBar.setTitle("Órden de servicio - " + no_orden);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Fetching order details from sqlite
        HashMap<String, String> order = db.getOrderDetails(no_orden);
        id_orden = order.get("id_orden");
        id_tiposervicio = order.get("id_tiposervicio");
        tiposervicio = order.get("tipo_servicio");
        String fecha_orden = order.get("fecha_orden");
        String fecha_inicio = order.get("fecha_inicio");
        String fecha_fin = order.get("fecha_fin");
        String falla_reportada = order.get("falla_reportada");
        String nombre_usuario = order.get("nombre_usuario");
        String nombre_supervisor = order.get("nombre_supervisor");
        String funcionando = order.get("funcionando");
        String status = order.get("status");

        integrado = order.get("integrado");

        sku = order.get("sku");
        sku_fabricante = order.get("sku_fabricante");
        id_ce = order.get("id_ce");
        modelo = order.get("modelo");

        sku_padre = order.get("sku_padre");

        // Setting values from sqlite to view xml
        txtTipoServicio = (TextView) findViewById(R.id.tiposervicio_view);
        txtTipoServicio.setText(tiposervicio);
        txtEquipo = (TextView) findViewById(R.id.equipo_view);
        txtEquipo.setText(sku_fabricante);
        //txtNoOrden = (TextView) findViewById(R.id.noorden_view);
        //txtNoOrden.setText(no_orden);
        txtFechaOrden = (TextView) findViewById(R.id.fechaorden_view);
        txtFechaOrden.setText(fecha_orden);
        txtFechaInicio = (TextView) findViewById(R.id.fechainicio_view);
        txtFechaInicio.setText(fecha_inicio);
        txtFechaFin = (TextView) findViewById(R.id.fechafin_view);
        txtFechaFin.setText(fecha_fin);
        txtFallaReportada = (TextView) findViewById(R.id.fallareportada_view);
        txtFallaReportada.setText(falla_reportada);
        txtNombreUsuario = (TextView) findViewById(R.id.nombreusuario_view);
        txtNombreUsuario.setText(nombre_usuario);
        txtNombreSupervisor = (TextView) findViewById(R.id.nombresupervisor_view);
        txtNombreSupervisor.setText(nombre_supervisor);
        txtFuncionando = (TextView) findViewById(R.id.funcionando_view);
        txtFuncionando.setText(funcionando);
        txtEstatus = (TextView) findViewById(R.id.status_view);
        txtEstatus.setText(status);
        do_routine = (Button) findViewById(R.id.do_routine);

        FloatingActionButton fabDoRoutine = (FloatingActionButton) findViewById(R.id.addRepairsList);
        fabDoRoutine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Launch List Orders Attended Activity
                Intent intent = new Intent(ManageOrderActivity.this,
                        ListViewRepairsActivity.class);
                intent.putExtra("no_orden", no_orden);
                startActivity(intent);
                finish();
            }
        });

        //Validación si es visible o no el botón
        if (status.equals("Abierta") ) {
            do_routine.setVisibility(View.VISIBLE);
            fabDoRoutine.setVisibility(View.VISIBLE);
            haveRoutine(id_tiposervicio);
        } else {
            do_routine.setVisibility(View.INVISIBLE);
            fabDoRoutine.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }

    /**
     * function to get JSON from API_REST about Routine by Model
     * */
    private void getRoutinesByModel(final String no_orden, final String sku, final boolean flagroutine, final String integrado, final String id_ce) {
        // Tag used to cancel the request
        String tag_string_req = "req_Routines";

        //pDialog.setMessage("Obteniendo Información...");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ROUTINES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Routines response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("routines");

                        for(int i=0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String rutinaid = jsonObject.optString("ID_MR").toString();
                            String tiposervicioid = jsonObject.optString("ID_TipoServicio").toString();
                            String modelo = jsonObject.optString("SKU").toString();
                            String orden = jsonObject.optString("Orden").toString();
                            String rutina = jsonObject.optString("Descripcion").toString();
                            String revisar = jsonObject.optString("Revisar").toString();
                            String color = jsonObject.optString("Color").toString();

                            // Inserting row in routines table
                            db.addRoutines(rutinaid, tiposervicioid, modelo, orden, rutina, revisar, color);

                        }
                            // Launch List Orders Attended Activity
                            Intent intent = new Intent(ManageOrderActivity.this,
                                    ListViewDoRoutineActivity.class);
                            intent.putExtra("no_orden", no_orden);
                            intent.putExtra("sku_fabricante", sku_fabricante);
                            intent.putExtra("flagroutine", flagroutine);
                            intent.putExtra("integrado", integrado);
                            intent.putExtra("sku_padre", sku_padre);
                            startActivity(intent);
                            finish();

                    } else {
                        // Error in orders. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("sku", sku);
                params.put("integrado", integrado);
                params.put("id_ce", id_ce);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to get JSON from API_REST about if Order haves Routine
     * */
    private void haveRoutine(final String id_tiposervicio) {
        // Tag used to cancel the request
        String tag_string_req = "req_HaveRoutine";

        //pDialog.setMessage("Obteniendo Información...");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_HAVEROUTINE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Have Routine response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("haveroutine");

                        for(int i=0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String tiposervicioid = jsonObject.optString("ID_TipoServicio").toString();
                            String tiposervicio = jsonObject.optString("Tipo_Servicio").toString();
                            String rutina = jsonObject.optString("Rutina").toString();

                            if (rutina.equals("1")) {
                                do_routine.setText("Realizar rutina");
                            } else {
                                do_routine.setText("Describir trabajo");
                            }

                            // Inserting row in have routine table
                            db.addHaveRoutine(tiposervicioid, tiposervicio, rutina);
                        }

                    } else {
                        // Error in orders. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_tiposervicio", id_tiposervicio);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /* Click al botón Realizar rutina */
    public void ClicDoRoutine (View view) {

        HashMap<String, String> haveroutine = db.getHaveRoutine();
        String tienerutina = haveroutine.get("rutina");

        if(tienerutina.equals("1")) {
            boolean flagroutine = true;
            if (no_orden.isEmpty()) {
                Toast.makeText(getApplicationContext(),
                        "Error al obtener el id de la órden!", Toast.LENGTH_LONG)
                        .show();
            } else {
                getRoutinesByModel(no_orden, sku, flagroutine, integrado, id_ce);
            }

        } else {

            // Launch List Orders Attended Activity
            Intent intent = new Intent(ManageOrderActivity.this,
                    ListViewDoRoutineActivity.class);
            intent.putExtra("no_orden", no_orden);
            intent.putExtra("sku_fabricante", sku_fabricante);
            intent.putExtra("flagroutine", false);
            intent.putExtra("integrado", integrado);
            intent.putExtra("sku_padre", sku_padre);
            startActivity(intent);
            finish();

        }
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteOrdersAttended();
        db.deleteRoutines();
        db.deleteHaveRoutine();
        db.deleteRepairs();
        // Launching the login activity
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }

}
