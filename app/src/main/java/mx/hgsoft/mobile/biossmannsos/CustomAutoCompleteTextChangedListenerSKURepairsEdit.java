package mx.hgsoft.mobile.biossmannsos;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ArrayAdapter;

public class CustomAutoCompleteTextChangedListenerSKURepairsEdit implements TextWatcher{

    public static final String TAG = "AutoCompleteSKURepairs.java";
    Context context;

    public CustomAutoCompleteTextChangedListenerSKURepairsEdit(Context context){
        this.context = context;
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTextChanged(CharSequence userInput, int start, int before, int count) {

        // if you want to see in the logcat what the user types
        Log.e(TAG, "User input: " + userInput);

        UpdateRepairsActivity updateRepairsActivity = ((UpdateRepairsActivity) context);

        // query the database based on the user input
        updateRepairsActivity.repair = updateRepairsActivity.getSKURepairsFromDb(userInput.toString());

        // update the adapater
        updateRepairsActivity.adapterSKURepairs.notifyDataSetChanged();
        updateRepairsActivity.adapterSKURepairs = new ArrayAdapter<String>(updateRepairsActivity, android.R.layout.simple_dropdown_item_1line, updateRepairsActivity.repair);
        updateRepairsActivity.autoCompleteViewSKURepairs.setAdapter(updateRepairsActivity.adapterSKURepairs);

    }

}