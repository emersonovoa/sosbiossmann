package mx.hgsoft.mobile.biossmannsos;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 29/04/16.
 */

public class ListViewDoRoutineActivity extends AppCompatActivity{

    private static final String TAG = ListViewDoRoutineActivity.class.getSimpleName();

    ListView listViewRoutines;
    ArrayList <String> checkedValue;
    Button button;

    public static String no_orden = "";
    public static String sku_fabricante = "";
    public static String flagroutine_string = "";

    public static boolean flagroutine;

    private static int routinesWithCheckbox = 0;

    private TextView txtEquipo;
    private TextView txtNoOrden;

    private EditText inputDescription;

    private SQLiteHandler db;
    private SessionManager session;

    ArrayList<Routines> routines;
    CustomListAdapterRoutines adapter;
    private ArrayList<String> selectedItems;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_do_routine);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        /* Getting parameters from ManageOrderActivity */
        Intent intent = getIntent();
        no_orden = intent.getExtras().getString("no_orden");
        sku_fabricante = intent.getExtras().getString("sku_fabricante");
        flagroutine = intent.getExtras().getBoolean("flagroutine");

        findViewsById();

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {
            logoutUser();
        }

        Cursor cursor = db.getRoutinesListCursor();
        cursor.moveToFirst();
        routines = new ArrayList<Routines>();

        while(!cursor.isAfterLast()) {
            if(cursor.getString(cursor.getColumnIndex("revisar")).equals("1")){
                routines.add(new Routines(cursor.getString(cursor.getColumnIndex("rutina")), true, false));
                routinesWithCheckbox = routinesWithCheckbox + 1;
            } else {
                routines.add(new Routines(cursor.getString(cursor.getColumnIndex("rutina")), false, false));
            }
            cursor.moveToNext();
        }
        cursor.close();

        adapter = new CustomListAdapterRoutines(ListViewDoRoutineActivity.this,routines);
        listViewRoutines.setAdapter(adapter);
        /*listViewRoutines.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                adapter.setCheckBox(position);

            }
        });*/

        txtNoOrden = (TextView) findViewById(R.id.noorden_routine_view);
        txtNoOrden.setText(no_orden);
        txtEquipo = (TextView) findViewById(R.id.equipo_routine_view);
        txtEquipo.setText(sku_fabricante);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), ManageOrderActivity.class);
        myIntent.putExtra("no_orden", no_orden);
        startActivityForResult(myIntent, 0);
        db.deleteRoutines();
        db.deleteHaveRoutine();
        routinesWithCheckbox = 0;
        return true;
    }

    private void findViewsById() {

        inputDescription = (EditText) findViewById(R.id.inputDescription);
        inputDescription.setVisibility(View.INVISIBLE);

        listViewRoutines = (ListView) findViewById(R.id.list_routines);
        listViewRoutines.setVisibility(View.INVISIBLE);

        checkedValue = new ArrayList<String>();

        if(flagroutine){listViewRoutines.setVisibility(View.VISIBLE);}else{inputDescription.setVisibility(View.VISIBLE);}
        button = (Button) findViewById(R.id.sendRoutine);
    }

    public void updateRoutine(View v) {

        if(flagroutine) {

            System.out.println("Rutinas a checar: " + routinesWithCheckbox);
            System.out.println("Items checked: " + adapter.itemsChecked());
            int size_selected = adapter.itemsChecked();

            if (routinesWithCheckbox == size_selected) {
                updateOrders(no_orden, flagroutine, "");
                Toast.makeText(getApplicationContext(),
                        "Se ha realizado la rutina de la órden: " + no_orden, Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Debe cumplirse con cada uno de los puntos de la rutina.", Toast.LENGTH_LONG)
                        .show();
            }

        } else {

            String descripcion_trabajo = inputDescription.getText().toString().trim();

            if ( checkValidation () ) {
                updateOrders(no_orden, flagroutine, descripcion_trabajo);
                Toast.makeText(getApplicationContext(),
                        "Se ha realizado las rutinas de la órden: " + no_orden, Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Describir el trabajo realizado!", Toast.LENGTH_LONG)
                        .show();
            }
        }

        // Create a bundle object
        //Bundle b = new Bundle();
        //b.putStringArray("selectedItems", outputStrArr);
        // Add the bundle to the intent.
        //intent.putExtras(b);
        // start the ResultActivity
        //startActivity(intent);
    }

    /**
     * Function to update order status in MySQL database will post params()
     */
    private void updateOrders(final String no_orden, final boolean flagroutine, final String descripcion_trabajo) {
        // Tag used to cancel the request
        String tag_string_req = "req_updateOrders";

        //pDialog.setMessage("Registering ...");
        //showDialog();

        if(flagroutine){flagroutine_string = "1";}else{flagroutine_string = "0";}
        System.out.println("flagroutine_string: " + flagroutine_string);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEORDERS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Update Response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // Launch login activity
                        Intent intent = new Intent(
                                ListViewDoRoutineActivity.this,
                                MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("no_orden", no_orden);
                params.put("flagroutine", flagroutine_string);
                params.put("descripcion_trabajo", descripcion_trabajo);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText(inputDescription)) ret = false;
        return ret;
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteOrdersAttended();
        db.deleteHaveRoutine();
        db.deleteRoutines();
        db.deleteRepairs();
        // Launching the login activity
        Intent intent = new Intent(ListViewDoRoutineActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        //doNothing();
    }

}
