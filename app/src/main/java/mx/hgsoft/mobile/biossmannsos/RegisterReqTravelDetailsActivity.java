package mx.hgsoft.mobile.biossmannsos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 10/05/16.
 */
public class RegisterReqTravelDetailsActivity extends AppCompatActivity{

    private static final String TAG = RegisterActivity.class.getSimpleName();

    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private SessionManager session;

    private Spinner spinnerViaticos;
    private EditText inputUnitario;
    private EditText inputTotal;
    private EditText inputObservaciones;

    private String svid = "";
    private String foliort = "";
    private String id_zona = "";
    private String no_noches = "";
    private String nPosSpinnerTravelsString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_reqtravel_details);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        /* Getting order number from List */
        foliort = getIntent().getExtras().getString("foliort");
        id_zona = getIntent().getExtras().getString("id_zona");
        no_noches = getIntent().getExtras().getString("no_noches");

        /* Getting xml elements */
        inputUnitario = (EditText) findViewById(R.id.unitario);
        //inputUnitario.setEnabled(false);
        inputTotal = (EditText) findViewById(R.id.total);
        //inputTotal.setEnabled(false);
        inputObservaciones = (EditText) findViewById(R.id.observaciones);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Fetching requests travrl details from sqlite
        HashMap<String, String> order = db.getRequestsTravelDetails(foliort);
        svid = order.get("svid");

        // Setting values from sqlite to view xml
        TextView txtFoliort = (TextView) findViewById(R.id.foliort_view);
        txtFoliort.setText(foliort);

        if (foliort.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el folio de la solicitud de viáticos!", Toast.LENGTH_LONG)
                    .show();
        } else {
            getCatKindOfTravel();
        }

        Button btnSaveDetails = (Button) findViewById(R.id.btnRegisterReqTravelDetails);

        // Register Button Click event
        btnSaveDetails.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                String spinnerTravelsString = null;
                spinnerTravelsString = spinnerViaticos.getSelectedItem().toString();
                int nPosTravelsSpinner = spinnerViaticos.getSelectedItemPosition();
                nPosSpinnerTravelsString = Integer.toString(nPosTravelsSpinner);
                /* End spinner travels */
                String unitario = inputUnitario.getText().toString().trim();
                String total = inputTotal.getText().toString().trim();
                String observaciones = inputObservaciones.getText().toString().trim();

                // Fetching user details from sqlite
                HashMap<String, String> user = db.getUserDetails();
                String usrMod = user.get("email");

                WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
                String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                if (checkValidation()) {
                    registerRequestsTravelDetails(svid, nPosSpinnerTravelsString, unitario, total, observaciones, usrMod, ip);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Llenar correctamente los campos!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), ListReqTravelDetailsActivity.class);
        myIntent.putExtra("foliort", foliort);
        db.deleteRequestsTravelDetails();
        db.deleteTravels();
        startActivityForResult(myIntent, 0);
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * function to get JSON from API_REST about Catalog's kind of travel
     */
    private void getCatKindOfTravel() {
        String tag_string_req = "req_catKindOfTravel";

        pDialog.setMessage("");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_KINDOFTRAVELS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Kind of travels response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("travels");

                        db.addCatTravels("0", "Elige tipo de viático");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String viaticoid = jsonObject.optString("ID_TipoViatico").toString();
                            String viatico = jsonObject.optString("Tipo_Viatico").toString();

                            // Inserting row in orders table
                            db.addCatTravels(viaticoid, viatico);
                        }
                        populatingSpinnerTravels();

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void populatingSpinnerTravels() {

            /* To populate Spinner, first create an adapter */
        spinnerViaticos = (Spinner) findViewById(R.id.viaticos_spinner);

        // The desired columns to be bound
        String[] columns = new String[]{
                "travel"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[]{
                android.R.id.text1
        };

        Cursor cursorTravels = db.getTravelsListCursor();

        SimpleCursorAdapter adapterTravels = new SimpleCursorAdapter(
                this, // context
                android.R.layout.simple_spinner_item, // layout file
                cursorTravels, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterTravels.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerViaticos.setAdapter(adapterTravels);
        spinnerViaticos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String str_position = Integer.toString(position);
                inputUnitario.setText("");
                inputTotal.setText("");
                if (str_position != "0") {
                    getCatTravelByZone(id_zona, str_position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    /**
     * function to get JSON from API_REST about Catalog's kind of travel
     */
    private void getCatTravelByZone(final String id_zone, final String idTravel) {
        // Tag used to cancel the request
        String tag_string_reqdetails = "req_requestsTravelByZone";



        pDialog.setMessage("Obteniendo Información...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REQUESTSIMPORTTRAVELS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Requests Import Travel By Zone response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("requestsTravelByZone");

                        String monto = "";

                        for(int i=0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            monto = jsonObject.optString("Monto").toString();
                        }

                        calculateImport(monto, no_noches);
                        hideDialog();

                    } else {
                        // Error in orders. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to requests travel url
                Map<String, String> params = new HashMap<String, String>();
                params.put("zone", id_zone);
                params.put("idTravel", idTravel);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_reqdetails);


    }


    /**
     * function to get JSON from API_REST about Catalog's kind of travel
     */
    private void calculateImport(final String monto, final String noches) {
        inputUnitario.setText(monto);
        double importe = Double.parseDouble(monto);
        double unit = Double.parseDouble(noches);
        double result = (double) (importe*unit);
        String total = String.valueOf(result);
        inputTotal.setText(total);

    }

    /**
     * Function to store requeststraveldetails in MySQL database will post
     * params(svid, nPosSpinnerTravelsString, unitario,
     * total, observaciones) to register url
     * */
    private void registerRequestsTravelDetails(final String svid, final String nPosSpinnerTravelsString, final String unitario, final String  total, final String observaciones, final String usrMod, final String ip) {
        // Tag used to cancel the request
        String tag_string_req = "req_registerRequestsTravelDetails";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTERREQUESTSTRAVELDETAILS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Repairs Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // Repair successfully stored in MySQL
                        Toast.makeText(getApplicationContext(), "Inserción Exitosa", Toast.LENGTH_LONG).show();
                        db.deleteRequestsTravelDetails();
                        db.deleteTravels();
                        // Launch ManageOrderActivity activity
                        Intent intent = new Intent(
                                RegisterReqTravelDetailsActivity.this,
                                ListReqTravelDetailsActivity.class);
                        intent.putExtra("foliort", foliort);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                //Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("svid", svid);
                params.put("tipo_viatico", nPosSpinnerTravelsString);
                params.put("unitario", unitario);
                params.put("total", total);
                params.put("observaciones", observaciones);
                params.put("usrMod", usrMod);
                params.put("ip", ip);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.selectedElementSpinner(spinnerViaticos)) {
            Toast.makeText(this, "Elije tipo de viático", Toast.LENGTH_SHORT).show();
            ret = false;
        }
        if (!Validation.hasText(inputUnitario)) ret = false;
        //if (!Validation.hasText(inputTotal)) ret = false;
        return ret;
    }

    private boolean checkPreValidation() {
        boolean ret = true;
        if (!Validation.selectedElementSpinner(spinnerViaticos)) {
            Toast.makeText(this, "Elije tipo de viático para obtener el Total.", Toast.LENGTH_SHORT).show();
            ret = false;
        }
        return ret;
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        db.deleteRequestsTravelDetails();
        db.deleteTravels();
        // Launching the login activity
        Intent intent = new Intent(RegisterReqTravelDetailsActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }

}
