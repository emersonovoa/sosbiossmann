package mx.hgsoft.mobile.biossmannsos;

/**
 * Created by resident on 05/07/16.
 */
import java.util.ArrayList;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class CustomListAdapterRoutines extends BaseAdapter{

    ArrayList<Routines> data;
    Activity context;
    boolean[] itemChecked;
    private static int counterChecked = 0;

    public CustomListAdapterRoutines(Activity context, ArrayList<Routines> routines) {
        super();
        this.context = context;
        this.data = routines;
        itemChecked = new boolean[routines.size()];
    }

    private class ViewHolder {
        TextView routineName;
        CheckBox ck1;
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public int itemsChecked() {return  counterChecked; }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        LayoutInflater inflater = context.getLayoutInflater();

        final int pos = position;
        Routines items = data.get(pos);
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_item_routines, null);
            holder = new ViewHolder();
            holder.routineName = (TextView) convertView.findViewById(R.id.routine_name);
            holder.ck1 = (CheckBox) convertView.findViewById(R.id.checkBox1);
            convertView.setTag(holder);
            //holder.ck1.setTag(data.get(position));

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        holder.routineName.setText(items.getName());
        holder.ck1.setChecked(true);

        if (itemChecked[position]) {
            holder.ck1.setChecked(true);
        }else {
            holder.ck1.setChecked(false);
        }

        if (items.haveCheckbox()){
            holder.ck1.setEnabled(true);
            holder.ck1.setVisibility(View.VISIBLE);
        } else {
            holder.ck1.setEnabled(false);
            holder.ck1.setVisibility(View.INVISIBLE);
        }

        holder.ck1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (holder.ck1.isChecked()) {
                    itemChecked[position] = true;
                    counterChecked = counterChecked + 1;
                } else {
                    itemChecked[position] = false;
                    counterChecked = counterChecked - 1;
                }
            }
        });

        return convertView;

    }



}