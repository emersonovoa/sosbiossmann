package mx.hgsoft.mobile.biossmannsos;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 12/05/16.
 */
public class TabGeneralRTFragment extends Fragment {

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private ManageRequestsTravelActivity manageRequestsTravel;

    View rootView;

    ProgressDialog pDialog;
    SQLiteHandler db;

    EditText viewFolio;
    Spinner spinnerCuentas;
    Spinner spinnerZonas;
    EditText inputFechaSalida;
    EditText inputFechaRegreso;
    EditText inputNoPersonas;
    Spinner spinnerTransportes;
    EditText inputObservaciones;
    Button btnSaveGeneral;

    Date date_fechasalida = new Date();
    Date date_fecharegreso = new Date();

    InputPassing mCallback;

    public interface InputPassing{
        public void sendInputs(String text);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (InputPassing) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement InputPassing");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_tab_general_rt, null);
        getAllWidgets(rootView);
        return rootView;
    }

    private void getAllWidgets(View view) {

        getCatCounts();
        //getCatZones();

        inputFechaSalida = (EditText) view.findViewById(R.id.fecha_salida);
        inputFechaSalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerOut();
            }
        });
        inputFechaRegreso = (EditText) view.findViewById(R.id.fecha_regreso);
        inputFechaRegreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerIn();
            }
        });
        inputNoPersonas = (EditText) view.findViewById(R.id.no_personas);

        //getCatTransports();

        inputObservaciones = (EditText) view.findViewById(R.id.observaciones);
        btnSaveGeneral = (Button) view.findViewById(R.id.btnRegisterRequestTravel);
        /*
        // Register Button Click event
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                saveForm(folio);
            }
        });*/
    }

    public void savingForm(String folio){
        mCallback.sendInputs(folio);
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }

    private void showDatePickerOut() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondateOut);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondateOut = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            inputFechaSalida.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(dayOfMonth));
        }
    };

    private void showDatePickerIn() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondateIn);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondateIn = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            inputFechaRegreso.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(dayOfMonth));
        }
    };

    public String getDateDiffString(Date dateOne, Date dateTwo)
    {
        long timeOne = dateOne.getTime();
        long timeTwo = dateTwo.getTime();
        long oneDay = 1000 * 60 * 60 * 24;
        long delta = (timeTwo - timeOne) / oneDay;
        if (delta > 0) {
            return "" + delta + "";
        }
        else {
            delta *= -1;
            return "" + delta + "";
        }
    }

    /**
     * function to get JSON from API_REST about Catalog's counts
     */
    private void getCatCounts() {
        String tag_string_req = "req_catCounts";
        //pDialog.setMessage("");
        //showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CATCOUNTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Counts response: " + response.toString());
                //hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("counts");
                        //db.addCatCounts("0", "Elige cuenta");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String cuentaid = jsonObject.optString("ID_Cuenta").toString();
                            String cuenta = jsonObject.optString("Cuenta").toString();
                            // Inserting row in orders table
                            db.addCatCounts(cuentaid, cuenta);
                        }
                        populatingSpinnerCounts(rootView);
                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getActivity(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to get JSON from API_REST about Catalog's zones
     */
    private void getCatZones() {
        String tag_string_req = "req_catZones";

        //pDialog.setMessage("");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CATZONES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Zones response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("zones");

                        db.addCatZones("0", "Elige zona");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String zonaid = jsonObject.optString("ID_Zona").toString();
                            String zona = jsonObject.optString("Zona").toString();

                            // Inserting row in orders table
                            db.addCatZones(zonaid, zona);
                        }
                        populatingSpinnerZones(rootView);

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getActivity(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to get JSON from API_REST about Catalog's transports
     */
    private void getCatTransports() {
        String tag_string_req = "req_catTransports";

        //pDialog.setMessage("");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_KINDTRANSPORTS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Transports response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("transports");

                        db.addCatTransports("0", "Elige transporte");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String transporteid = jsonObject.optString("ID_TipoTransporte").toString();
                            String transporte = jsonObject.optString("Tipo_Transporte").toString();

                            // Inserting row in orders table
                            db.addCatTransports(transporteid, transporte);
                        }
                        populatingSpinnerTransports(rootView);

                    } else {
                        // Error in counts. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getActivity(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void populatingSpinnerCounts(View view) {

            /* To populate Spinner, first create an adapter */
        spinnerCuentas = (Spinner) view.findViewById(R.id.cuenta_spinner);

        // The desired columns to be bound
        String[] columns = new String[]{
                "count"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[]{
                android.R.id.text1
        };

        Cursor cursorCounts = db.getCountsListCursor();

        SimpleCursorAdapter adapterCounts = new SimpleCursorAdapter(
                getActivity(), // context
                android.R.layout.simple_spinner_item, // layout file
                cursorCounts, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterCounts.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerCuentas.setAdapter(adapterCounts);
    }

    private void populatingSpinnerZones(View view) {
        spinnerZonas = (Spinner) view.findViewById(R.id.zona_spinner);
        String[] columns = new String[]{
                "zone"
        };
        int[] to = new int[]{
                android.R.id.text1
        };
        Cursor cursorZones = db.getZonesListCursor();

        SimpleCursorAdapter adapterZones = new SimpleCursorAdapter(
                getActivity(), // context
                android.R.layout.simple_spinner_item, // layout file
                cursorZones, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterZones.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerZonas.setAdapter(adapterZones);
    }

    private void populatingSpinnerTransports(View view) {
        spinnerTransportes = (Spinner) view.findViewById(R.id.tipotransporte_spinner);
        String[] columns = new String[]{
                "transport"
        };
        int[] to = new int[]{
                android.R.id.text1
        };
        Cursor cursorTransports = db.getTransportsListCursor();

        SimpleCursorAdapter adapterTransports = new SimpleCursorAdapter(
                getActivity(), // context
                android.R.layout.simple_spinner_item, // layout file
                cursorTransports, // DB cursor
                columns, // data to bind to the UI
                to, // views that'll represent the data from `columns`
                0
        );
        adapterTransports.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Create the list view and bind the adapter
        spinnerTransportes.setAdapter(adapterTransports);
    }
}