package mx.hgsoft.mobile.biossmannsos.app;

/**
 * Created by resident on 14/03/16.
 */
public class AppConfig {

    //Local
    // Server user login url
    //Petition http://casaplarre.mx/sos/app_sos_api/
    public static String URL_LOGIN = "http://casaplarre.mx/sos/app_sos_api/login.php";

    // Server user orders attended
    //http://casaplarre.mx/sos/app_sos_api/ordersAttended.php
    public static String URL_ORDERSATTENDED = "http://casaplarre.mx/sos/app_sos_api/getOrdersAttended.php";

    // Server user orders no attended
    //http://casaplarre.mx/sos/app_sos_api/ordersNoAttended.php
    public static String URL_ORDERSNOATTENDED = "http://casaplarre.mx/sos/app_sos_api/getOrdersNoAttended.php";

    // Server all orders by user
    //http://casaplarre.mx/sos/app_sos_api/ordersByUser.php
    public static String URL_ALLORDERS = "http://casaplarre.mx/sos/app_sos_api/getOrdersByUser.php";

    // Server all rotuines
    //http://casaplarre.mx/sos/app_sos_api/routines.php
    public static String URL_ROUTINES = "http://casaplarre.mx/sos/app_sos_api/getRoutines.php";

    // Server order haves rotuine?
    public static String URL_HAVEROUTINE = "http://casaplarre.mx/sos/app_sos_api/haveRoutine.php";

    // Server all repairs
    //http://casaplarre.mx/sos/app_sos_api/repairsByOrder.php
    public static String URL_REPAIRS = "http://casaplarre.mx/sos/app_sos_api/getRepairsByOrder.php";

    // Server update orders status
    //http://casaplarre.mx/sos/app_sos_api/update.php
    public static String URL_UPDATEORDERS = "http://casaplarre.mx/sos/app_sos_api/updateOrders.php";

    // Server register repairs
    //http://casaplarre.mx/sos/app_sos_api/repairsAdd.php
    public static String URL_REGISTERREPAIRS = "http://casaplarre.mx/sos/app_sos_api/registerRepairs.php";

    // Server update repairs
    public static String URL_UPDATEREPAIRS = "http://casaplarre.mx/sos/app_sos_api/updateRepairs.php";

    // Server register requests travels
    //http://casaplarre.mx/sos/app_sos_api/formRegisterRequestsTravel.php
    public static String URL_REGISTERREQUESTSTRAVEL = "http://casaplarre.mx/sos/app_sos_api/registerRequestsTravel.php";

    // Server register requests travels details
    //http://casaplarre.mx/sos/app_sos_api/
    public static String URL_REGISTERREQUESTSTRAVELDETAILS = "http://casaplarre.mx/sos/app_sos_api/registerRequestsTravelDetails.php";

    // Server register requests travels tickets
    //http://casaplarre.mx/sos/app_sos_api/
    public static String URL_REGISTERREQUESTSTRAVELTICKETS = "http://casaplarre.mx/sos/app_sos_api/registerRequestsTravelTickets.php";

    // Server register requests travels vouchers
    //http://casaplarre.mx/sos/app_sos_api/
    public static String URL_REGISTERREQUESTSTRAVELVOUCHERS = "http://casaplarre.mx/sos/app_sos_api/registerRequestsTravelVouchers.php";

    // Server requests travel by ST
    //http://casaplarre.mx/sos/app_sos_api/requestsTravel.php
    public static String URL_REQUESTSTRAVEL = "http://casaplarre.mx/sos/app_sos_api/getRequestsTravel.php";

    //http://casaplarre.mx/sos/app_sos_api/requestsTravelLast.php
    public static String URL_REQUESTSTRAVELLAST = "http://casaplarre.mx/sos/app_sos_api/getRequestsTravelLast.php";

    // Server requests travel details by ID SV
    //http://casaplarre.mx/sos/app_sos_api/
    public static String URL_REQUESTSTRAVELDETAILS = "http://casaplarre.mx/sos/app_sos_api/getRequestsTravelDetails.php";

    // Server requests import travel by ID_ZONA and ID_VIATICO
    //http://casaplarre.mx/sos/app_sos_api/
    public static String URL_REQUESTSIMPORTTRAVELS = "http://casaplarre.mx/sos/app_sos_api/getRequestsTravelByZone.php";

    // Server requests travel details by ID SV
    //http://casaplarre.mx/sos/app_sos_api/
    public static String URL_REQUESTSTRAVELAIRPLANETICKETS = "http://casaplarre.mx/sos/app_sos_api/getRequestsTravelTickets.php";

    // Server requests travel vouchers by ID SV
    //http://casaplarre.mx/sos/app_sos_api/
    public static String URL_REQUESTSTRAVELVOUCHERS = "http://casaplarre.mx/sos/app_sos_api/getRequestsTravelVouchers.php";

    /* Catalogs */
    // Count's catalog
    public static String URL_CATCOUNTS = "http://casaplarre.mx/sos/app_sos_api/getCounts.php";

    // Zone's catalog
    public static String URL_CATZONES = "http://casaplarre.mx/sos/app_sos_api/getZones.php";

    // Kind Transports catalog
    public static String URL_KINDTRANSPORTS = "http://casaplarre.mx/sos/app_sos_api/getKindTransports.php";

    // Kind of travel catalog
    public static String URL_KINDOFTRAVELS = "http://casaplarre.mx/sos/app_sos_api/getKindTravel.php";

    // Airports catalog
    public static String URL_AIRPORTS = "http://casaplarre.mx/sos/app_sos_api/getAirports.php";

    // Vouchers catalog
    public static String URL_CATVOUCHERS = "http://casaplarre.mx/sos/app_sos_api/getCatVouchers.php";

    // Providers catalog
    public static String URL_PROVIDERS = "http://casaplarre.mx/sos/app_sos_api/getProviders.php";

    // Upload files server storage
    public static String URL_UPLOADS = "http://casaplarre.mx/sos/app_sos_api/UploadToServer.php";

    // Path from image profiles users
    public static String URL_PROFILE_IMGS = "http://casaplarre.mx/upload/usuarios/fotos/";

    // Server all sku repairs by order sku
    public static String URL_SKU_REPAIRS = "http://casaplarre.mx/sos/app_sos_api/getSKURepairs.php";

}
