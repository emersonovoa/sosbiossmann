package mx.hgsoft.mobile.biossmannsos;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ArrayAdapter;

public class CustomAutoCompleteTextChangedListenerSKURepairs implements TextWatcher{

    public static final String TAG = "AutoCompleteSKURepairs.java";
    Context context;

    public CustomAutoCompleteTextChangedListenerSKURepairs(Context context){
        this.context = context;
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTextChanged(CharSequence userInput, int start, int before, int count) {

        // if you want to see in the logcat what the user types
        Log.e(TAG, "User input: " + userInput);

        RegisterRepairsActivity registerRepairsActivity = ((RegisterRepairsActivity) context);

        // query the database based on the user input
        registerRepairsActivity.repair = registerRepairsActivity.getSKURepairsFromDb(userInput.toString());

        // update the adapater
        registerRepairsActivity.adapterSKURepairs.notifyDataSetChanged();
        registerRepairsActivity.adapterSKURepairs = new ArrayAdapter<String>(registerRepairsActivity, android.R.layout.simple_dropdown_item_1line, registerRepairsActivity.repair);
        registerRepairsActivity.autoCompleteViewSKURepairs.setAdapter(registerRepairsActivity.adapterSKURepairs);

    }

}