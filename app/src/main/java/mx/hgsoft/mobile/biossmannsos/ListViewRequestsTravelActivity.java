package mx.hgsoft.mobile.biossmannsos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import mx.hgsoft.mobile.biossmannsos.activity.LoginActivity;
import mx.hgsoft.mobile.biossmannsos.activity.RegisterActivity;
import mx.hgsoft.mobile.biossmannsos.app.AppConfig;
import mx.hgsoft.mobile.biossmannsos.app.AppController;
import mx.hgsoft.mobile.biossmannsos.helper.SQLiteHandler;
import mx.hgsoft.mobile.biossmannsos.helper.SessionManager;

/**
 * Created by resident on 1/05/16.
 */
public class ListViewRequestsTravelActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private SimpleCursorAdapter dataAdapter;
    private SessionManager session;

    private String id_user_st = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests_travel_list);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.addInList);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Launch Manage Requests Travel Activity
                Intent intent = new Intent(ListViewRequestsTravelActivity.this,
                        RegisterRequestsTravelActivity.class);
                startActivity(intent);
            }
        });

        // Activate SQLite database handler
        db = new SQLiteHandler(getApplicationContext());
        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // First delete all info from catálagos
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();

        // Fetching user details from sqlite
        HashMap<String, String> user = db.getUserDetails();
        id_user_st = user.get("uid");
        if (id_user_st.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener el id del usuario!", Toast.LENGTH_LONG)
                    .show();
        } else {
            getRequestsTravelByST(id_user_st);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }

    /**
     * function to get JSON from API_REST about Requests Travel by ST
     * */
    private void getRequestsTravelByST(final String id_user_st) {
        // Tag used to cancel the request
        String tag_string_req = "req_requestsTravel";

        //pDialog.setMessage("Obteniendo Información...");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REQUESTSTRAVEL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Requests Travel response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Get the instance of JSONArray that contains JSONObjects
                        JSONArray jsonArray = jObj.optJSONArray("requestsTravel");

                        for(int i=0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String svid = jsonObject.optString("ID_SV").toString();
                            String folio = jsonObject.optString("Folio").toString();
                            String fecha_solicitud = jsonObject.optString("Fecha_Solicitud").toString();
                            String cuenta = jsonObject.optString("Cuenta").toString();
                            String id_zona = jsonObject.optString("ID_Zona").toString();
                            String zona = jsonObject.optString("Zona").toString();
                            String fecha_salida = jsonObject.optString("Fecha_Salida").toString();
                            String fecha_regreso = jsonObject.optString("Fecha_Regreso").toString();
                            String no_noches = jsonObject.optString("No_Noches").toString();
                            String no_personas = jsonObject.optString("No_Personas").toString();
                            String tipo_transporte = jsonObject.optString("TipoTransporte").toString();
                            String anticipo = jsonObject.optString("Anticipo").toString();
                            String gastos_avion = jsonObject.optString("Gastos_Avion").toString();
                            String gastos_comprobados = jsonObject.optString("Gastos_Comprobados").toString();
                            String observaciones = jsonObject.optString("Observaciones").toString();
                            String status = jsonObject.optString("Status").toString();
                            String fecha_mod = jsonObject.optString("Fecha_Mod").toString();
                            String usr_mod = jsonObject.optString("Usr_Mod").toString();
                            String ip_mod = jsonObject.optString("IP_Mod").toString();

                            // Inserting row in orders table
                            db.addRequestsTravelByST(svid, folio, fecha_solicitud, cuenta, id_zona, zona,
                                    fecha_salida, fecha_regreso, no_noches, no_personas, tipo_transporte,
                                    anticipo, gastos_avion, gastos_comprobados, observaciones, status,
                                    fecha_mod, usr_mod, ip_mod);
                        }
                        //Generate ListView from SQLite Database
                        displayListView();

                    } else {
                        // Error in orders. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to requests travel url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_st", id_user_st);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void displayListView() {
        Cursor cursor = db.getRequestsTravelListCursor();

        // The desired columns to be bound
        String[] columns = new String[] {
                "foliort",
                "fecha_solicitud",
                "zona",
                "fecha_salida",
                "fecha_regreso",
                "gastos_comprobados",
                "statustr"
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[] {
                R.id.foliort,
                R.id.fecha_solicitud,
                R.id.zona,
                R.id.fecha_salida,
                R.id.fecha_regreso,
                R.id.gastos_comprobados,
                R.id.statustr
        };

        // create the adapter using the cursor pointing to the desired data
        //as well as the layout information
        dataAdapter = new SimpleCursorAdapter(
                this, R.layout.rows_requests_travel,
                cursor,
                columns,
                to,
                0);

        ListView listView = (ListView) findViewById(R.id.listviewrequeststravel);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                // Get the cursor, positioned to the corresponding row in the result set
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);

                // Get the folio number from this row in the database.
                String folioNumber = cursor.getString(cursor.getColumnIndexOrThrow("foliort"));
                // Launch Manage Requests Travel Activity
                Intent intent = new Intent(ListViewRequestsTravelActivity.this,
                        ManageRequestsTravelActivity.class);
                intent.putExtra("foliort", folioNumber);
                startActivity(intent);
            }
        });



        EditText myFilter = (EditText) findViewById(R.id.myFilter);
        myFilter.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                dataAdapter.getFilter().filter(s.toString());
            }
        });

        dataAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence constraint) {
                try {
                    return db.fetchRequestsTravelByNumber(constraint.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        db.deleteRequestsTravel();
        db.deleteCounts();
        db.deleteZones();
        db.deleteTransports();
        // Launching the login activity
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {

        //doNothing();

    }

}
